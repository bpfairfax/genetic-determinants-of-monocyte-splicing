
library(gplots)
library(RColorBrewer)
library(rtracklayer)
library(DESeq2)
library(plyr)
library(FactoMineR)
library("pheatmap") 
library(reshape)
library(ggplot2)
library(ggrepel)
library(XGR)

name='IFN'
nameC0='gene_raw_readcount_IFN.txt'
nameC1='gene_raw_readcount_UT.txt'


#----- READ INPUT FILES -----
#View(unique(gtf_df$gene_biotype))

Destiny_Folder0 <- paste("/folder/", nameC0, sep='')
treatment <- read.table(Destiny_Folder0, sep = "", stringsAsFactors = FALSE, header = T, quote=NULL)

Destiny_Folder1 <- paste("/folder/", nameC1, sep='')
control <- read.table(Destiny_Folder1, sep = "", stringsAsFactors = FALSE, header = T, quote=NULL)

countData <- merge(treatment, control, by = "id", all = TRUE)
countData <- na.omit(countData)
row.names(countData) <- countData$id
countData <- countData[,-1]

colnames(countData) <- gsub(name,name,colnames(countData))

out <- strsplit(as.character(colnames(countData)),'_') 
Sample_ID <- as.data.frame( do.call(rbind, out) )
colnames(Sample_ID) <- c('treatment', 'id')
dim(Sample_ID)
dim(countData)

Sample_ID_dup <- Sample_ID[Sample_ID$id %in% Sample_ID$id[duplicated(Sample_ID[c("id")])],]
countData <- countData[,Sample_ID$id %in% Sample_ID$id[duplicated(Sample_ID[c("id")])]]

dim(Sample_ID_dup)
dim(countData)

#DEG
coldata <- DataFrame(Treatment=relevel(factor(Sample_ID_dup$treatment), ref = "UT"))
dds <- DESeqDataSetFromMatrix(countData = countData,
                                  colData = coldata,
                                  design = ~ Treatment)

## ----prefilter-----------------------------------------------------------
#- Zero reads
# keep <- rowSums(counts(dds)) >= 10
# dds <- dds[keep,]

#----- DEG analysis ----------------------------------------------
dds <- DESeq(dds, fitType='local')
resultsNames(dds)
resTime_point_CT <- results(dds, pAdjustMethod = "BH", contrast=c("treatment", "UT", name))
resTime_point_CT$gene_id <- rownames(resTime_point_CT) 
resTime_point_CT <- count(resTime_point_CT)

#------------- ID to name --------------
annotation <- rtracklayer::import('/GRCh38.gtf')
gtf_DF <- as.data.frame(annotation)
gtf_DF <- gtf_DF[which( gtf_DF$type == 'gene'),]
annotations_CT <- merge(resTime_point_CT, gtf_DF, by='gene_id')

dim(resTime_point_CT)
dim(annotations_CT)
 
## ----resOrder------------------------------------------------------------
DEG_CT <- as.data.frame(annotations_CT[order(annotations_CT$padj),])

#--------- Transcript types ------------------

FREq_CT <- count(DEG_CT[DEG_CT$padj <= 0.05,], 'gene_biotype')

#----------------------------------- Save results -----------------------------------------
# setwd('/folder/')
# dir.create(name)
setwd(paste('/folder/',name,sep=''))
write.table(DEG_CT, paste("DEG_CT_", paste(name,'.txt',sep=''), sep=""), row.names= FALSE, quote=FALSE, sep = "\t")
write.table(FREq_CT, paste("transcript_types_CT_", paste(name,'.txt',sep=''), sep=""), row.names= FALSE, quote=FALSE, sep = "\t")
 