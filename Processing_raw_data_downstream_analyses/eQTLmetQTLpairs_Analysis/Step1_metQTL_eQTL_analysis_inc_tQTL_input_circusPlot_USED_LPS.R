

#=============================== met-exp correlation
library(data.table)
methExprs = fread('/Volumes/Isar_Nassiri_Fairfax_lab/KEY-FILES/Articles_Monocyte_eQTL/Article_ciseQTLs/eQTL_mQTL_pairs_analysis/LPS24correlationMethExprs_results.txt', stringsAsFactors = F, header = T)
methExprs[which(methExprs$exprs == 'CD55'),]
methExprs_sub = methExprs[which(methExprs$adj.P.Val<0.001),]
#  beta: coefficient of the methylation change

#=============================== metQTL all
metQTL_LPS24 = fread('/Volumes/Isar_Nassiri_Fairfax_lab/RESULTS_USED/methyl_eQTL/metQTL/QTLtools/nominal_imputedGenotype_all_LPS_annotated.txt', stringsAsFactors = F)
dim(metQTL_LPS24)
colnames(metQTL_LPS24)[1] = c('cpg_ID')

metQTL_LPS24_sub = metQTL_LPS24[metQTL_LPS24$FDR < 0.001,]
dim(metQTL_LPS24_sub)
head(metQTL_LPS24_sub)
metQTL_LPS24_sub$SNP_ID = gsub('.*;', '', metQTL_LPS24_sub$SNP_ID)
table(metQTL_LPS24_sub$chrID)

#=============================== eQTL peak LPS

eQTL_LPS24 = fread('/Volumes/Isar_Nassiri_Fairfax_lab/RESULTS_USED/eQTL/eQTL_LPS24_annotated_clumped_leadSNPs.txt', stringsAsFactors = F)
eQTL_LPS24_sub = eQTL_LPS24[eQTL_LPS24$FDR < 0.001,]
dim(eQTL_LPS24_sub)

#=============================== eQTL_mQTL_all

colnames(eQTL_LPS24_sub)[match(c("FDR", "slop"), colnames(eQTL_LPS24_sub))] = c("FDR_eQTL", "slope_eQTL")
colnames(metQTL_LPS24_sub)[match(c("FDR", "slope"), colnames(metQTL_LPS24_sub))] = c("FDR_mQTL", "slope_mQTL")

eQTL_mQTL_all = merge(eQTL_LPS24_sub, metQTL_LPS24_sub, by = 'SNP_ID')
dim(eQTL_mQTL_all)
head(eQTL_mQTL_all)

eQTL_mQTL_all = as.data.frame(eQTL_mQTL_all)
eQTL_mQTL_all = eQTL_mQTL_all[,-which(colnames(eQTL_mQTL_all) == 'index')]

#=============================== eQTL_mQTL_corr all

eQTL_mQTL_all$index = paste(eQTL_mQTL_all$cpg_ID, eQTL_mQTL_all$gene_name, sep = '_')
methExprs_sub$index = paste(methExprs_sub$cpg, methExprs_sub$exprs, sep = '_')

eQTL_mQTL_corr = merge(eQTL_mQTL_all, methExprs_sub, by = 'index')
dim(eQTL_mQTL_corr)

eQTL_mQTL_corr$index2 = paste(eQTL_mQTL_corr$SNP_ID, eQTL_mQTL_corr$gene_id, sep = '_')

#=============================== eQTL peak Naive
library(data.table)
eQTL_Naive = fread('/Volumes/Isar_Nassiri_Fairfax_lab/RESULTS_USED/eQTL/eQTL_UT_annotated_clumped_leadSNPs.txt', stringsAsFactors = F)
eQTL_Naive_sub = eQTL_Naive[eQTL_Naive$FDR < 0.001,]
dim(eQTL_Naive_sub)
eQTL_Naive_sub$index = paste(eQTL_Naive_sub$SNP_ID, eQTL_Naive_sub$gene_id, sep = '_')

#=============================== eQTL_mQTL_corr CS  
eQTL_mQTL_corr_CS = eQTL_mQTL_corr[-which(eQTL_mQTL_corr$index2 %in% eQTL_Naive_sub$index),]
colnames(eQTL_mQTL_corr_CS)
table(eQTL_mQTL_corr_CS$seqnames)
dim(eQTL_mQTL_corr_CS)

#=============================== coloc per SNP
library(coloc)

ok=0
j=1
for(j in 1:(dim(eQTL_mQTL_corr_CS)[1]))
{
COLOC_Per_SNP=NULL
COLOC_Per_SNP <- coloc.abf(list(pvalues=as.numeric(eQTL_mQTL_corr_CS$FDR_eQTL[j]), N=176, MAF=as.numeric(eQTL_mQTL_corr_CS$maf[j]), type="quant"), 
                           list(pvalues=as.numeric(eQTL_mQTL_corr_CS$FDR_mQTL[j]), N=176, MAF=as.numeric(eQTL_mQTL_corr_CS$maf[j]), type="quant"))

temp = data.frame(eQTL_mQTL_corr_CS[j,], PP.H4.abf = as.numeric(COLOC_Per_SNP$summary['PP.H4.abf']))

  if(j==1 & ok == 0){RESULTS = temp; ok=1}
  if(j!=1 & ok == 0){RESULTS = temp; ok=1}
  if(j!=1 & ok == 1){RESULTS=rbind(temp,RESULTS)} 
print(j)
}

table(RESULTS$PP.H4.abf>0.75)

library(data.table)
fwrite(RESULTS, '/Volumes/Isar_Nassiri_Fairfax_lab/KEY-FILES/Articles_Monocyte_eQTL/Article_ciseQTLs/eQTL_mQTL_pairs_analysis/eQTL_mQTL_corr_CS_coloc_LPS24.txt', quote = F, row.names = F, sep = '\t')
dim(RESULTS)

#=============================== eQTL_mQTL_corr_CS_coloc + no corr in naive
# eQTL_mQTL_corr_CS_coloc = fread('/Volumes/Isar_Nassiri_Fairfax_lab/KEY-FILES/Articles_Monocyte_eQTL/Article_ciseQTLs/eQTL_mQTL_pairs_analysis/eQTL_mQTL_corr_CS_coloc.txt', stringsAsFactors = F ,header = T)
# 
# eQTL_mQTL_corr_CS_coloc = eQTL_mQTL_corr_CS_coloc[eQTL_mQTL_corr_CS_coloc$PP.H4.abf>0.75,]
# dim(eQTL_mQTL_corr_CS_coloc)
# 
# treatment='UT'
# met_exp_UT = fread(paste0('/Volumes/Fairfaxlab1/Laptop/Article_figures/Step63_transQTL_QTLtools/Exp_Met_analysis/',treatment,'correlationMethExprs_results.txt'), stringsAsFactors = F ,header = T)
# range(met_exp_UT$adj.P.Val)
# met_exp_UT = met_exp_UT[met_exp_UT$adj.P.Val < 0.001,]
# 
# met_exp_UT$index = paste(met_exp_UT$cpg, met_exp_UT$exprs, sep = '_')
# 
# eQTL_mQTL_corr_CS_coloc_selected = eQTL_mQTL_corr_CS_coloc[-which(eQTL_mQTL_corr_CS_coloc$index %in% met_exp_UT$index),]
# dim(eQTL_mQTL_corr_CS_coloc_selected)
# 
# library(data.table)
# fwrite(eQTL_mQTL_corr_CS_coloc_selected, '/Volumes/Isar_Nassiri_Fairfax_lab/KEY-FILES/Articles_Monocyte_eQTL/Article_ciseQTLs/eQTL_mQTL_pairs_analysis/eQTL_mQTL_corr_CS_coloc_selected.txt', quote = F, row.names = F, sep = '\t')
# 
# eQTL_mQTL_corr_CS_coloc_selected[(eQTL_mQTL_corr_CS_coloc_selected$exprs == 'CD55'),]

#----------------- do I have any trans-effect or no
# head(featureData)
# dim(featureData)
# featureData_sub2 = featureData
# colnames(featureData_sub2) = paste0('gene_',colnames(featureData_sub2))
# colnames(featureData_sub2)[1] = 'exprs'
# 
# methExprs_annotated = merge(methExprs_sub, featureData_sub2, by = 'exprs')
# 
# dim(cpg_featureData)
# cpg_featureData_sub2 = cpg_featureData
# colnames(cpg_featureData_sub2) = paste0('cpg_',colnames(cpg_featureData_sub2))
# colnames(cpg_featureData_sub2)[1] = 'cpg'
# 
# methExprs_annotated = merge(methExprs_annotated, cpg_featureData_sub2, by = 'cpg')
# 
# methExprs_annotated$cpg_chromosome = gsub('_.*','',methExprs_annotated$cpg_chromosome)
# table(methExprs_annotated$gene_chromosome == methExprs_annotated$cpg_chromosome)
# methExprs_annotated_distal = methExprs_annotated[which(methExprs_annotated$gene_chromosome != methExprs_annotated$cpg_chromosom),]
# 

#--- CHECK PVALUE FOR FIGURE
# eQTL_mQTL_corr_CS_coloc = fread('/Volumes/Isar_Nassiri_Fairfax_lab/KEY-FILES/Articles_Monocyte_eQTL/Article_ciseQTLs/eQTL_mQTL_pairs_analysis/eQTL_mQTL_corr_CS_coloc.txt', stringsAsFactors = F ,header = T)
# eQTL_mQTL_corr_CS_coloc[which(eQTL_mQTL_corr_CS_coloc$cpg_ID == 'cg22687766' &  eQTL_mQTL_corr_CS_coloc$SNP_ID == 'rs2914937'),]
# 
# 
# MethyQTL_UT = fread('/Volumes/Isar_Nassiri_Fairfax_lab/KEY-FILES/Articles_Monocyte_eQTL/Article_ciseQTLs/eQTL_mQTL_pairs_analysis/nominal_imputedGenotype_all_UT_annotated.txt', stringsAsFactors = F, header = T, fill = T)
# MethyQTL_UT$SNP_ID = gsub('.*;', '', MethyQTL_UT$SNP_ID)
# MethyQTL_UT[which(MethyQTL_UT$CpG == 'cg22687766' &  MethyQTL_UT$SNP_ID == 'rs2914937'),]

#=============================== cicus plot visualization
library(data.table)
eQTL_mQTL_corr_CS_coloc = fread('/Volumes/Isar_Nassiri_Fairfax_lab/KEY-FILES/Articles_Monocyte_eQTL/Article_ciseQTLs/eQTL_mQTL_pairs_analysis/eQTL_mQTL_corr_CS_coloc.txt', stringsAsFactors = F ,header = T)
dim(eQTL_mQTL_corr_CS_coloc)
length(unique(eQTL_mQTL_corr_CS_coloc$SNP_ID))

table(eQTL_mQTL_corr_CS_coloc$PP.H4.abf>0.75)
dim(eQTL_mQTL_corr_CS_coloc)
table(paste0('chr', eQTL_mQTL_corr_CS_coloc$seqnames[order(eQTL_mQTL_corr_CS_coloc$seqnames)]))

eQTL_mQTL_corr_CS_coloc = eQTL_mQTL_corr_CS_coloc[eQTL_mQTL_corr_CS_coloc$PP.H4.abf>0.75,]
dim(eQTL_mQTL_corr_CS_coloc)
 
eQTL_mQTL_corr_CS_coloc = eQTL_mQTL_corr_CS_coloc[order(eQTL_mQTL_corr_CS_coloc$bpval, decreasing = F),]
eQTL_mQTL_corr_CS_coloc = eQTL_mQTL_corr_CS_coloc[!duplicated(eQTL_mQTL_corr_CS_coloc$gene_id),]
dim(eQTL_mQTL_corr_CS_coloc)

eQTL_mQTL_corr_CS_coloc = as.data.frame(eQTL_mQTL_corr_CS_coloc)
table(eQTL_mQTL_corr_CS_coloc[,colnames(eQTL_mQTL_corr_CS_coloc) == 'seqnames'])
eQTL_mQTL_corr_CS_coloc[,colnames(eQTL_mQTL_corr_CS_coloc) == 'seqnames'] = factor(eQTL_mQTL_corr_CS_coloc[,colnames(eQTL_mQTL_corr_CS_coloc) == 'seqnames'], levels = c(1:22))
colnames(eQTL_mQTL_corr_CS_coloc)
dim(eQTL_mQTL_corr_CS_coloc)
table(eQTL_mQTL_corr_CS_coloc$seqnames)
eQTL_mQTL_corr_CS_coloc = eQTL_mQTL_corr_CS_coloc[with(eQTL_mQTL_corr_CS_coloc, order(seqnames, start.x)),]
colnames(eQTL_mQTL_corr_CS_coloc)

#=============================== input files for visualization
setwd('/Users/isar.nassiri/Desktop/Analysis_FairfaxLab/KEY-FILES/Articles_Monocyte_eQTL/Article_ciseQTLs/FIGURES/Figure-3/supportive/')

#--- tQTL
tQTL_LPS24 = fread('/Volumes/Isar_Nassiri_Fairfax_lab/RESULTS_USED/tQTL/tQTL_LPS24_annotated_clumped_leadSNPs.txt', stringsAsFactors = F)
tQTL_LPS24_sub = tQTL_LPS24[tQTL_LPS24$FDR < 0.001,]
dim(tQTL_LPS24)

tQTL_LPS24$index_tQTL = paste(tQTL_LPS24$SNP_ID, tQTL_LPS24$gene_name, sep = '_')
eQTL_mQTL_corr_CS_coloc$index_tQTL = paste(eQTL_mQTL_corr_CS_coloc$SNP_ID, eQTL_mQTL_corr_CS_coloc$gene_name, sep = '_')

dim(eQTL_mQTL_corr_CS_coloc)
dim(tQTL_LPS24)

library(dplyr)
eQTL_mQTL_corr_CS_coloc_tQTL = left_join(eQTL_mQTL_corr_CS_coloc, tQTL_LPS24[,c("gene_id", "FDR", "seqnames", "start", "end", "index_tQTL")], by = 'index_tQTL')
dim(eQTL_mQTL_corr_CS_coloc_tQTL)

tQTL_LPS24$index = paste(tQTL_LPS24$SNP_ID, tQTL_LPS24$gene_name, sep = '_')
eQTL_mQTL_corr_CS_coloc$index = paste(eQTL_mQTL_corr_CS_coloc$SNP_ID, eQTL_mQTL_corr_CS_coloc$gene_name, sep = '_')

length(intersect(tQTL_LPS24$SNP_ID, eQTL_mQTL_corr_CS_coloc$SNP_ID))

library(dplyr)
eQTL_mQTL_corr_CS_coloc_tQTL = left_join(eQTL_mQTL_corr_CS_coloc, tQTL_LPS24[,c("gene_id", "FDR", "seqnames", "start", "end", "index")], by = 'index')
dim(eQTL_mQTL_corr_CS_coloc_tQTL)
colnames(eQTL_mQTL_corr_CS_coloc_tQTL)

#------ labels
 
gene_label = eQTL_mQTL_corr_CS_coloc_tQTL[which(eQTL_mQTL_corr_CS_coloc_tQTL$FDR_eQTL < 1e-35),c('seqnames.x','start.x','end.x',"gene_name")]
CPG_label = eQTL_mQTL_corr_CS_coloc_tQTL[which(eQTL_mQTL_corr_CS_coloc_tQTL$FDR_eQTL < 1e-35),c('chrID','start.y','end.y',"cpg")]

colnames(gene_label) = c('chr1',	'start1',	'end1', 'gene_name')
colnames(CPG_label) = c('chr1',	'start1',	'end1',	'gene_name')

dim(gene_label)
dim(CPG_label)

head(gene_label)

# eQTL_mQTL_corr_CS_coloc_tQTL = eQTL_mQTL_corr_CS_coloc_tQTL[!is.na(eQTL_mQTL_corr_CS_coloc_tQTL$gene_id.y),]
# eQTL_mQTL_corr_CS_coloc_tQTL = eQTL_mQTL_corr_CS_coloc_tQTL[which(eQTL_mQTL_corr_CS_coloc_tQTL$FDR<1e-12),]
# transcript_tQTL = eQTL_mQTL_corr_CS_coloc_tQTL[,c('seqnames.y','start','end', "gene_id.y")]
# colnames(transcript_tQTL) = c('chr1',	'start1',	'end1',	'gene_name')

labels = do.call(rbind, list(gene_label, CPG_label)) #transcript_tQTL,

labels = labels[!duplicated(labels$gene_name),]
labels$chr1 = paste0('chr',labels$chr1)
dim(labels)
length(labels$gene_name)
table(labels$chr1)
dim(table(labels$chr1))
write.csv(labels,paste0('label_data.csv'), row.names = F)
 
#=============================== add correlation coefficient
library(data.table)
expmatrix <- fread(paste0('/Volumes/Isar_Nassiri_Fairfax_lab/RESULTS_USED/Expression/Input_files_gene/expression_',treatment,'.txt'), stringsAsFactors = F, header = T)
gene_name_id <- fread('/Volumes/Isar_Nassiri_Fairfax_lab/RESULTS_USED/Biomart_DB_gene_name_id.txt', stringsAsFactors = F, header = T)
colnames(gene_name_id) = c('symbol', 'id')
Expression = merge(gene_name_id, expmatrix, by = 'id')

Methylation <- fread(paste0("/Volumes/Isar_Nassiri_Fairfax_lab/RESULTS_USED/methyl_eQTL/Methylation_profile_",treatment,".bed.gz"), stringsAsFactors = F, header = T)

colnames(Methylation)
colnames(Expression)

Methylation = as.data.frame(Methylation)
row.names(Methylation) = make.names(Methylation$pid, unique = T)

Expression = as.data.frame(Expression)
row.names(Expression) = make.names(Expression$symbol, unique = T)

Expression = Expression[,which(colnames(Expression) %in% colnames(Methylation))]
Methylation = Methylation[,which(colnames(Methylation) %in% colnames(Expression))]

identical(colnames(Expression), colnames(Methylation))

for(i in 1:dim(eQTL_mQTL_corr_CS_coloc)[1])
{
  print(i)
  Methylation_sub = Methylation[which(row.names(Methylation) == eQTL_mQTL_corr_CS_coloc$cpg_ID[i]),]
  Expression_sub = Expression[which(row.names(Expression) == eQTL_mQTL_corr_CS_coloc$gene_name[i]),]
  eQTL_mQTL_corr_CS_coloc$CC[i] = cor(as.numeric(Expression_sub), as.numeric(Methylation_sub))
}

range(eQTL_mQTL_corr_CS_coloc_tQTL$FDR[!is.na(eQTL_mQTL_corr_CS_coloc_tQTL$FDR)])
range(eQTL_mQTL_corr_CS_coloc_tQTL$FDR_eQTL[!is.na(eQTL_mQTL_corr_CS_coloc_tQTL$FDR_eQTL)])
range(eQTL_mQTL_corr_CS_coloc_tQTL$FDR_mQTL[!is.na(eQTL_mQTL_corr_CS_coloc_tQTL$FDR_mQTL)])
range(eQTL_mQTL_corr_CS_coloc$CC)

#--- tracks
table(is.na(eQTL_mQTL_corr_CS_coloc_tQTL$FDR))

data_point=data.frame(chr=paste0('chr',eQTL_mQTL_corr_CS_coloc_tQTL$seqnames.y),
                      start=abs(eQTL_mQTL_corr_CS_coloc_tQTL$start),
                      end= abs(eQTL_mQTL_corr_CS_coloc_tQTL$end),
                      value1=-log10(eQTL_mQTL_corr_CS_coloc_tQTL$FDR))
data_point = data_point[!is.na(data_point$value1),]
range(data_point$value1)
dim(data_point)
write.csv(data_point,paste0('point_tQTL.csv'), row.names = F)

data_point=data.frame(chr=paste0('chr',eQTL_mQTL_corr_CS_coloc$seqnames),
                      start=abs(eQTL_mQTL_corr_CS_coloc$start.y),
                      end= abs(eQTL_mQTL_corr_CS_coloc$end.y),
                      value1=-log10(eQTL_mQTL_corr_CS_coloc$FDR_mQTL))
data_point = data_point[!is.na(data_point$value1),]
range(data_point$value1)
dim(data_point)
write.csv(data_point,paste0('point_metQTL.csv'), row.names = F)

data_point=data.frame(chr=paste0('chr',eQTL_mQTL_corr_CS_coloc$seqnames),
                      start=abs(eQTL_mQTL_corr_CS_coloc$start.x),
                      end= abs(eQTL_mQTL_corr_CS_coloc$end.x),
                      value1=-log10(eQTL_mQTL_corr_CS_coloc$FDR_eQTL))
data_point = data_point[!is.na(data_point$value1),]
range(data_point$value1)
dim(data_point)
write.csv(data_point,paste0('point_eQTL.csv'), row.names = F)

data_point=data.frame(chr=paste0('chr',eQTL_mQTL_corr_CS_coloc$seqnames),
                      start=abs(eQTL_mQTL_corr_CS_coloc$start.x),
                      end= abs(eQTL_mQTL_corr_CS_coloc$end.x),
                      value1=eQTL_mQTL_corr_CS_coloc$CC)
range(eQTL_mQTL_corr_CS_coloc$CC)
write.csv(data_point,paste0('point_correlation.csv'), row.names = F)

#---- CytoBand
library(RCircos)
data("UCSC.HG38.Human.CytoBandIdeogram")
chro.bands = UCSC.HG38.Human.CytoBandIdeogram
colnames(chro.bands) = c('chr',	'start',	'end',	'value1',	'value2')
chro.bands$chr = as.character(chro.bands$chr)
# chro.bands = chro.bands[-which(chro.bands$chr %in% c('chrX')),]
table(chro.bands$chr)
write.csv(chro.bands,'Hg38_chromosome_cytoband.csv',row.names = F)
getwd()

#-- parameters
#99CC33
#FFCC33
#CCCC99

