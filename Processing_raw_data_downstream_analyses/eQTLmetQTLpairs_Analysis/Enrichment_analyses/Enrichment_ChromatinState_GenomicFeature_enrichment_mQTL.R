
# setwd(dirname(rstudioapi::getActiveDocumentContext()$path))
zScore <- function (cntA, totA, cntB, totB) {
  #calculate
  avgProportion <- (cntA + cntB) / (totA + totB)
  probA <- cntA/totA
  probB <- cntB/totB
  SE <- sqrt(avgProportion * (1-avgProportion)*(1/totA + 1/totB))
  zScore <- (probA-probB) / SE
  return (zScore)
}

treatment = 'UT'
dir.create(paste0('/t1-data/data/fairfaxlab/transcriptomics/RNAseq/eQTL/monocytes/analysis/QTL_analysis/Fastqtl/Monocyte_Paper_related_files/mQTL/',treatment,'/'))
setwd(paste0('/t1-data/data/fairfaxlab/transcriptomics/RNAseq/eQTL/monocytes/analysis/QTL_analysis/Fastqtl/Monocyte_Paper_related_files/mQTL/',treatment,'/'))

#--------------------------- read inputs

library(data.table)
library(GenomicRanges)
# https://egg2.wustl.edu/roadmap/data/byFileType/chromhmmSegmentations/ChmmModels/coreMarks/jointModel/final/download/
states_bed = fread("/t1-data/data/fairfaxlab/transcriptomics/RNAseq/eQTL/monocytes/analysis/DataBases/Genotyping1000/E029_15_coreMarks_hg38lift_dense.bed")  # hg19

chromatin_states = GRanges(sample = states_bed[[4]],
                           seqnames = gsub('chr','',states_bed[[1]]),
                           ranges = IRanges(states_bed[[2]], states_bed[[3]]),
                           state = states_bed[[4]])
length(chromatin_states)
head(chromatin_states)

genomic_regions = fread('/t1-data/data/fairfaxlab/transcriptomics/RNAseq/eQTL/monocytes/analysis/DataBases/genomic_regions_DB_TxDb_Biomart_K562_hg38.txt', stringsAsFactors = F, header = T)
genomic_regions$seqnames = gsub('chr','',genomic_regions$seqnames)
genomic_regions = genomic_regions[which(genomic_regions$seqnames %in% c(1:22, 'X')),]
genomic_regions = GRanges(sample = genomic_regions$sample,
                          seqnames = genomic_regions$seqnames,
                          ranges = IRanges(genomic_regions$start, genomic_regions$end),
                          state = genomic_regions$state)

SNPs_Imputed = fread('/t1-data/data/fairfaxlab/transcriptomics/RNAseq/eQTL/monocytes/analysis/QTL_analysis/Fastqtl/RESULTS_USED/Genotype/MAF_imputed_Allsamples_TYPED.txt')
SNPs_Imputed = as.data.frame(SNPs_Imputed)
SNPs_Imputed_subject <- GRanges(Rle(as.character(SNPs_Imputed$CHROM)), IRanges(SNPs_Imputed$POS, width=2), rsID = SNPs_Imputed$ID)

library(data.table)
profile = fread(paste0('/t1-data/data/fairfaxlab/transcriptomics/RNAseq/eQTL/monocytes/analysis/QTL_analysis/Fastqtl/methylationQTL_QTLtools/Premutation_chunks_imputedGenotype_',treatment,'/Premutated_imputedGenotype_all_',treatment,'_annotated.txt'), stringsAsFactors = F)
profile = profile[profile$qval < 1e-3,]
colnames(profile)
range(abs(profile$Distance))
profile$SNP_ID = gsub('.*;', '', profile$SNP_ID)

library(ieugwasr) #R/3.5.0-newgcc
clump_input = cbind.data.frame(rsid = profile$SNP_ID, chr_name = profile$SNP_chr, chrom_start = profile$SNP_start, pval = profile$qval)
r <- sapply(clump_input, is.factor)
clump_input[r] <- lapply(clump_input[r], as.character)
str(clump_input)
clumped_input <- ld_clump(clump_input)
head(clumped_input)
profile = profile[which(profile$SNP_ID %in% clumped_input$rsid),]
dim(profile)

eQTL = fread(paste0('/t1-data/data/fairfaxlab/transcriptomics/RNAseq/eQTL/monocytes/analysis/QTL_analysis/Fastqtl/RESULTS_USED/methylationQTL/QTLtools/nominal_imputedGenotype_all_', treatment, '_annotated.txt'), stringsAsFactors = F)
eQTL = data.frame(eQTL)
eQTL = eQTL[eQTL$FDR < 1e-3,]
colnames(eQTL)
range(abs(eQTL$Distance))
eQTL = eQTL[which(eQTL$CpG %in% profile$CpG),]

genes = unique(eQTL$CpG)
length(genes)

i=1
#--------------------------------- CS enrichment
# for (i in 1:length(genes)) {

print(genes[i])
print(i)
#--- select eQTLs
eQTL_sub = eQTL[which(eQTL$CpG == genes[i]),]
dim(eQTL_sub)

#--- Foreground_rsIDs
Foreground_rsIDs <- GRanges(paste0(eQTL_sub$SNP_chr,':',eQTL_sub$SNP_start,'-',eQTL_sub$SNP_start), rsID = eQTL_sub$SNP_ID)
length(Foreground_rsIDs)

posQuerySNP <- GRanges(paste0(eQTL_sub$chrID[1],':',eQTL_sub$start[1],'-',eQTL_sub$start[1])) + 100000

#--- Background_rsIDs
fo <- findOverlaps(query=posQuerySNP, subject=SNPs_Imputed_subject, type="any")
Background_rsIDs = SNPs_Imputed_subject[subjectHits(fo),]

#--- remove foreground from background
Background_rsIDs = Background_rsIDs[-which(Background_rsIDs$rsID %in% Foreground_rsIDs$rsID),]
length(Background_rsIDs)

#--- enrich foreground
fo <- findOverlaps(query=Foreground_rsIDs, subject=chromatin_states, type="any")
foreground_CS = chromatin_states[subjectHits(fo),]
#--- enrich background
fo <- findOverlaps(query=Background_rsIDs, subject=chromatin_states, type="any")
Background_CS = chromatin_states[subjectHits(fo),]

CSs = unique(foreground_CS$sample)

for(t in 1:length(CSs))
{
  cntA=length(which(foreground_CS$sample==CSs[t]))
  totA=length(Foreground_rsIDs)
  cntB=length(which(Background_CS$sample==CSs[t]))
  totB=length(Background_rsIDs)
  
  table = c(cntA, totA, cntB, totB)
  dim(table)<-c(2,2)
  fishert <- fisher.test(table)
  
  z_score = zScore(cntA, totA, cntB, totB)
  
  temp = data.frame(gene = genes[i], CS = CSs[t], p = fishert$p.value, oddsratio = fishert$estimate, CIl= fishert$conf.int[1], CIh = fishert$conf.int[2], zScore = z_score, FE = cntA, FA = totA, BE = cntB, BA = totB, treatment = treatment)
  
  if(t==1){RESULTs = temp}else{RESULTs = rbind(RESULTs, temp)}
  
  print(z_score)
}
# if(i==1){RESULTs2 = RESULTs}else{RESULTs2 = rbind(RESULTs2, RESULTs)}

#------------------- genomic features

#--- enrich foreground
fo <- findOverlaps(query=Foreground_rsIDs, subject=genomic_regions, type="any")
foreground_GR = genomic_regions[subjectHits(fo),]

#--- enrich background
fo <- findOverlaps(query=Background_rsIDs, subject=genomic_regions, type="any")
Background_GR = genomic_regions[subjectHits(fo),]

if(length(foreground_GR)>0)
{
  GRs = unique(foreground_GR$sample)
  for(t in 1:length(GRs))
  {
    cntA=length(which(foreground_GR$sample==GRs[t]))
    totA=length(foreground_GR)
    cntB=length(which(Background_GR$sample==GRs[t]))
    totB=length(Background_GR)
    
    table = c(cntA, totA, cntB, totB)
    dim(table)<-c(2,2)
    fishert <- fisher.test(table)
    
    z_score = zScore(cntA, totA, cntB, totB)
    
    temp = data.frame(gene = genes[i], GR = GRs[t], p = fishert$p.value, oddsratio = fishert$estimate, CIl= fishert$conf.int[1], CIh = fishert$conf.int[2], zScore = z_score, FE = cntA, FA = totA, BE = cntB, BA = totB, treatment = treatment)
    
    if(t==1){RESULTsGR = temp}else{RESULTsGR = rbind(RESULTsGR, temp)}
    
    print(z_score)
  }
  # if(i==1){RESULTsGR2 = RESULTsGR}else{RESULTsGR2 = rbind(RESULTsGR2, RESULTsGR)}
}# is.null
# }

write.table(RESULTs, paste0('CS_Enrichment_Result_',genes[i],'_',treatment,'.txt'), quote = F, row.names = F, sep = '\t')
write.table(RESULTsGR, paste0('GR_Enrichment_Result_',genes[i],'_',treatment,'.txt'), quote = F, row.names = F, sep = '\t')

