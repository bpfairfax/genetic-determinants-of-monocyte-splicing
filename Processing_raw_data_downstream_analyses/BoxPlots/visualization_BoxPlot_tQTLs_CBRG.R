library(dplyr)
library( ggpubr)
library(data.table)

#===== some of the SNP ids are subset of others and the fread retrive the first one (e.g. rs570631764 and rs570631764000); therefore I add _ to the SNP id to distinguish them.
# library(data.table)
# TP_GENOTYPE = fread(paste0('/t1-data/data/fairfaxlab/transcriptomics/RNAseq/eQTL/monocytes/analysis/QTL_analysis/Fastqtl/RESULTS_USED/Genotype/Monocyte_imputed_matrixQTL_Allsamples_justSNPs_format2.txt'), stringsAsFactors = F)
# revised_ids=paste0(TP_GENOTYPE$id, '_')
# TP_GENOTYPE2 = cbind(revised_ids, TP_GENOTYPE[,-1])
# TP_GENOTYPE2[1:10,1:10]
# colnames(TP_GENOTYPE2)
# write.table(TP_GENOTYPE2, paste0('/t1-data/data/fairfaxlab/transcriptomics/RNAseq/eQTL/monocytes/analysis/QTL_analysis/Fastqtl/RESULTS_USED/Genotype/Monocyte_imputed_matrixQTL_Allsamples_justSNPs_format2_USED_for_BOXPLOT.txt'),quote = F, row.names = F, sep = '\t')

SNPs = fread('/t1-data/data/fairfaxlab/transcriptomics/RNAseq/eQTL/monocytes/analysis/QTL_analysis/Fastqtl/RESULTS_USED/Genotype//MAF_imputed_Allsamples_revised.txt', stringsAsFactors = F)
colnames(SNPs)[3] = 'SNP_ID'

incorporate.PC = data.frame(IFN=6,LPS24=5,UT=7)

treatment = 'UT'

#---------- multiple input
# query = fread(paste0('/t1-data/data/fairfaxlab/transcriptomics/RNAseq/eQTL/monocytes/analysis/QTL_analysis/Fastqtl/RESULTS_USED/input_files_replication_ciseQTL/CStQTL_',treatment,'_traits_colocalization.txt'), stringsAsFactors = F)
# dim(query)
# query = as.data.frame(query)
# query = query[query$PP.H4.abf > 0.75,] 
# query = query[order(query$pval.exposure, decreasing = F),]
# query_dedup = query[!duplicated(query$SNP),]
# 
# colnames(query)[which(colnames(query) == 'SNP')] = 'SNP_ID'
# merged=merge(query, tQTL, by = 'SNP_ID')
# fwrite(merged, paste0('/t1-data/data/fairfaxlab/transcriptomics/RNAseq/eQTL/monocytes/analysis/QTL_analysis/Fastqtl/RESULTS_USED/input_files_replication_ciseQTL/CStQTL_',treatment,'_traits_colocalization_merged.txt'), quote = F, row.names = F, sep = '\t')
# 
# tQTL = fread(paste0('/t1-data/data/fairfaxlab/transcriptomics/RNAseq/eQTL/monocytes/analysis/QTL_analysis/Fastqtl/RESULTS_USED/tQTL/Window_100kb/Peak_SNPs_LD_tQTL_',treatment,'.txt'), stringsAsFactors = F)
# tQTL = tQTL[which(tQTL$SNP_ID %in% query_dedup$SNP),]
# dim(tQTL)
# input = data.frame(SNP_ID=tQTL$SNP_ID,gene_name=tQTL$gene_name,gene_id=tQTL$gene_id, stringsAsFactors = F)

#----------

#---------- destination folder of plots
folder = paste0('/t1-data/data/fairfaxlab/transcriptomics/RNAseq/eQTL/monocytes/analysis/QTL_analysis/Fastqtl/RESULTS_USED/input_files_replication_ciseQTL/tQTL_GWAS/',treatment,'/')  
 
#---------- single input
# input = data.frame(SNP_ID='rs807612',gene_name='DDX1',gene_id='ENST00000233084', stringsAsFactors = F)
# input = data.frame(SNP_ID='rs4713600',gene_name='PSMB9 ',gene_id='ENST00000374859', stringsAsFactors = F)
# input = data.frame(SNP_ID='rs2305789',gene_name='EIF3G ',gene_id='ENST00000253108', stringsAsFactors = F)
# input = data.frame(SNP_ID='rs3771569',gene_name='HDLBP ',gene_id='ENST00000391976', stringsAsFactors = F)
# input = data.frame(SNP_ID='rs3771569',gene_name='HDLBP ',gene_id='ENST00000422933', stringsAsFactors = F)
# input = data.frame(SNP_ID='rs10735079',gene_name='OAS1',gene_id='ENST00000452357', stringsAsFactors = F)
# input = data.frame(SNP_ID='rs181225830',gene_name='CCR2',gene_id='ENST00000292301', stringsAsFactors = F)
# input = data.frame(SNP_ID='rs138137175',gene_name='DPP9',gene_id='ENST00000601764', stringsAsFactors = F)

#---------- check if gene is exist in the expression profile
treatment='IFN'
TP_id = fread(paste0('/t1-data/data/fairfaxlab/transcriptomics/RNAseq/eQTL/monocytes/analysis/QTL_analysis/Fastqtl/RESULTS_USED/Expression/Input_files_transcript/expression_',treatment,'.txt'),select='id',stringsAsFactors = F) # includes IFB1
which(input$gene_id %in% TP_id$id)
 

PC = T
 
#---------- add more annotation to the input
input = input[order(input$gene_name),]
library(dplyr)
input = left_join(input, SNPs, by = 'SNP_ID')
t=1

# for (t in 1:dim(input)[1]) {
# 
# tryCatch({

SNP = input$SNP_ID[t]
TRANSCRIPT = input$gene_id[t]
GENE = input$gene_name[t]

print(GENE)

Allele_0 = paste0(input$REF[t], input$REF[t])
Allele_1 = paste0(input$REF[t], input$ALT[t])
Allele_2 = paste0(input$ALT[t], input$ALT[t])

TP_GENOTYPE = fread(paste0('/t1-data/data/fairfaxlab/transcriptomics/RNAseq/eQTL/monocytes/analysis/QTL_analysis/Fastqtl/RESULTS_USED//Genotype/Monocyte_imputed_matrixQTL_Allsamples_justSNPs_format2_USED_for_BOXPLOT.txt'),skip = paste0(SNP, '_'), nrows = 1, stringsAsFactors = F)
TP_GENOTYPE$V1 = gsub('_', '', TP_GENOTYPE$V1)

GENOTYPE_sample_names = fread(paste0('/t1-data/data/fairfaxlab/transcriptomics/RNAseq/eQTL/monocytes/analysis/QTL_analysis/Fastqtl/RESULTS_USED//Genotype/Monocyte_imputed_matrixQTL_Allsamples_justSNPs_format2.txt'),nrows = 1, stringsAsFactors = F)
colnames(TP_GENOTYPE) = colnames(GENOTYPE_sample_names)

#------------------------------------------- UT
treatment='UT'
library(data.table)
TP_EXPESSION = fread(paste0('/t1-data/data/fairfaxlab/transcriptomics/RNAseq/eQTL/monocytes/analysis/QTL_analysis/Fastqtl/RESULTS_USED/Expression/Input_files_transcript/expression_',treatment,'.txt'),skip = TRANSCRIPT, nrows = 1,stringsAsFactors = F) # includes IFB1
EXPESSION_sample_names = fread(paste0('/t1-data/data/fairfaxlab/transcriptomics/RNAseq/eQTL/monocytes/analysis/QTL_analysis/Fastqtl/RESULTS_USED/Expression/Input_files_transcript/expression_',treatment,'.txt'),nrows = 1,stringsAsFactors = F) # includes IFB1
colnames(TP_EXPESSION) = colnames(EXPESSION_sample_names)

TP_EXPESSION = TP_EXPESSION[,which(colnames(TP_EXPESSION) %in% colnames(TP_GENOTYPE)),with=F]
TP_GENOTYPE_treatment = TP_GENOTYPE[,which(colnames(TP_GENOTYPE) %in% colnames(TP_EXPESSION)),with=F]
dim(TP_GENOTYPE_treatment)
dim(TP_EXPESSION)

identical(colnames(TP_EXPESSION) , colnames(TP_GENOTYPE_treatment))

dim(TP_EXPESSION)
dim(TP_GENOTYPE_treatment)

INPUT = rbind(as.matrix(TP_GENOTYPE_treatment), TP_EXPESSION)
INPUT = t(INPUT)
dim(INPUT)
colnames(INPUT) = c('genotype',GENE)
INPUT = as.data.frame(INPUT)

INPUT = INPUT[-1,]

INPUT[,1] = as.numeric(as.character(INPUT[,1]))
table(INPUT[,1])
INPUT[,2] = as.numeric(as.character(INPUT[,2]))

#INPUT[,2] = INPUT[,2]+10
summary(INPUT[,2])
#------------- regress out

message("Calculating PC")
nPC = incorporate.PC[,(colnames(incorporate.PC) == treatment)]

if(t==1 & PC)
{
expression.data = fread(paste0('/t1-data/data/fairfaxlab/transcriptomics/RNAseq/eQTL/monocytes/analysis/QTL_analysis/Fastqtl/RESULTS_USED/Expression/Input_files_transcript/expression_',treatment,'.txt'),stringsAsFactors = F) # includes IFB1
dim(expression.data)
expression.data = expression.data[,which(colnames(expression.data) %in% colnames(TP_GENOTYPE_treatment)),with=F]

identical(colnames(expression.data), colnames(TP_GENOTYPE_treatment))

variance<-apply(expression.data[,-1],1,var)

test.cov<-prcomp(t(expression.data[!variance==0,-1]),center=T,scale.=T)
test.cov<-data.frame(test.cov$x[1:length(expression.data[!variance==0,-1]),1:length(expression.data[!variance==0,-1])])
test.cov_UT<-test.cov[,1:nPC]
}

data <- data.frame(probe=INPUT[,2], genotype=INPUT[,1], test.cov_UT)

pcFit <- lm(data$probe ~ data$genotype + as.matrix(test.cov_UT))
summary(pcFit)
remodelled <- data$probe - rowSums(sapply(1:nPC, function(i) pcFit$coefficients[i+2]*test.cov_UT[,i]))
summary(remodelled)

# cor(remodelledd10, remodelledm10)
# summary(INPUT[,2]/10)
# summary(remodelled/10)

INPUT[,2] = remodelled

#-------------

INPUT$genotype[which(INPUT$genotype == 0)] = Allele_0
INPUT$genotype[which(INPUT$genotype == 1)] = Allele_1
INPUT$genotype[which(INPUT$genotype == 2)] = Allele_2

INPUT$genotype = factor(INPUT$genotype, levels = c(Allele_0,Allele_1,Allele_2))


x <- paste("INPUT_",treatment, sep="") # INPUT_UT
eval(call("<-", as.name(x), INPUT))

#------------------------------------------- LPS24
treatment='LPS24'
library(data.table)
TP_EXPESSION = fread(paste0('/t1-data/data/fairfaxlab/transcriptomics/RNAseq/eQTL/monocytes/analysis/QTL_analysis/Fastqtl/RESULTS_USED/Expression/Input_files_transcript/expression_',treatment,'.txt'),skip = TRANSCRIPT, nrows = 1,stringsAsFactors = F) # includes IFB1
EXPESSION_sample_names = fread(paste0('/t1-data/data/fairfaxlab/transcriptomics/RNAseq/eQTL/monocytes/analysis/QTL_analysis/Fastqtl/RESULTS_USED/Expression/Input_files_transcript/expression_',treatment,'.txt'),nrows = 1,stringsAsFactors = F) # includes IFB1
colnames(TP_EXPESSION) = colnames(EXPESSION_sample_names)

TP_EXPESSION = TP_EXPESSION[,which(colnames(TP_EXPESSION) %in% colnames(TP_GENOTYPE)),with=F]
TP_GENOTYPE_treatment = TP_GENOTYPE[,which(colnames(TP_GENOTYPE) %in% colnames(TP_EXPESSION)),with=F]
dim(TP_GENOTYPE_treatment)
dim(TP_EXPESSION)

identical(colnames(TP_EXPESSION) , colnames(TP_GENOTYPE_treatment))

dim(TP_EXPESSION)
dim(TP_GENOTYPE_treatment)

INPUT = rbind(as.matrix(TP_GENOTYPE_treatment), TP_EXPESSION)
INPUT = t(INPUT)
dim(INPUT)
colnames(INPUT) = c('genotype',GENE)
INPUT = as.data.frame(INPUT)

INPUT = INPUT[-1,]

INPUT[,1] = as.numeric(as.character(INPUT[,1]))
table(INPUT[,1])
INPUT[,2] = as.numeric(as.character(INPUT[,2]))
summary((INPUT[,2]))

table(INPUT[,2] < 1)
#INPUT[,2] = INPUT[,2]+10
summary(INPUT[,2])
#------------- regress out

message("Calculating PC")
nPC = incorporate.PC[,(colnames(incorporate.PC) == treatment)]

if(t==1 & PC)
{
  expression.data = fread(paste0('/t1-data/data/fairfaxlab/transcriptomics/RNAseq/eQTL/monocytes/analysis/QTL_analysis/Fastqtl/RESULTS_USED/Expression/Input_files_transcript/expression_',treatment,'.txt'),stringsAsFactors = F) # includes IFB1
  dim(expression.data)
  expression.data = expression.data[,which(colnames(expression.data) %in% colnames(TP_GENOTYPE_treatment)),with=F]
  
  identical(colnames(expression.data), colnames(TP_GENOTYPE_treatment))
  
  variance<-apply(expression.data[,-1],1,var)
  
  test.cov<-prcomp(t(expression.data[!variance==0,-1]),center=T,scale.=T)
  test.cov<-data.frame(test.cov$x[1:length(expression.data[!variance==0,-1]),1:length(expression.data[!variance==0,-1])])
  test.cov_LPS<-test.cov[,1:nPC]
}
data <- data.frame(probe=INPUT[,2], genotype=INPUT[,1], test.cov_LPS)

pcFit <- lm(data$probe ~ data$genotype + as.matrix(test.cov_LPS))
summary(pcFit)
remodelled <- data$probe - rowSums(sapply(1:nPC, function(i) pcFit$coefficients[i+2]*test.cov_LPS[,i]))
summary(INPUT[,2])
summary(remodelled)

INPUT[,2] = remodelled
summary(remodelled)
summary(INPUT[,2])
 
#-------------

INPUT$genotype[which(INPUT$genotype == 0)] = Allele_0
INPUT$genotype[which(INPUT$genotype == 1)] = Allele_1
INPUT$genotype[which(INPUT$genotype == 2)] = Allele_2

INPUT$genotype = factor(INPUT$genotype, levels = c(Allele_0,Allele_1,Allele_2))

x <- paste("INPUT_",treatment, sep="") # INPUT_UT
eval(call("<-", as.name(x), INPUT))

#------------------------------------------- IFN
treatment='IFN'
library(data.table)
TP_EXPESSION = fread(paste0('/t1-data/data/fairfaxlab/transcriptomics/RNAseq/eQTL/monocytes/analysis/QTL_analysis/Fastqtl/RESULTS_USED/Expression/Input_files_transcript/expression_',treatment,'.txt'),skip = TRANSCRIPT, nrows = 1,stringsAsFactors = F) # includes IFB1
EXPESSION_sample_names = fread(paste0('/t1-data/data/fairfaxlab/transcriptomics/RNAseq/eQTL/monocytes/analysis/QTL_analysis/Fastqtl/RESULTS_USED/Expression/Input_files_transcript/expression_',treatment,'.txt'),nrows = 1,stringsAsFactors = F) # includes IFB1
colnames(TP_EXPESSION) = colnames(EXPESSION_sample_names)

TP_EXPESSION = TP_EXPESSION[,which(colnames(TP_EXPESSION) %in% colnames(TP_GENOTYPE)),with=F]
TP_GENOTYPE_treatment = TP_GENOTYPE[,which(colnames(TP_GENOTYPE) %in% colnames(TP_EXPESSION)),with=F]
dim(TP_GENOTYPE_treatment)
dim(TP_EXPESSION)

identical(colnames(TP_EXPESSION) , colnames(TP_GENOTYPE_treatment))

dim(TP_EXPESSION)
dim(TP_GENOTYPE_treatment)

INPUT = rbind(as.matrix(TP_GENOTYPE_treatment), TP_EXPESSION)
INPUT = t(INPUT)
dim(INPUT)
colnames(INPUT) = c('genotype',GENE)
INPUT = as.data.frame(INPUT)

INPUT = INPUT[-1,]

INPUT[,1] = as.numeric(as.character(INPUT[,1]))
table(INPUT[,1])
INPUT[,2] = as.numeric(as.character(INPUT[,2]))
#INPUT[,2] = INPUT[,2]+10
summary(INPUT[,2])

#------------- regress out

message("Calculating PC")
nPC = incorporate.PC[,(colnames(incorporate.PC) == treatment)]

if(t==1 & PC)
{
  expression.data = fread(paste0('/t1-data/data/fairfaxlab/transcriptomics/RNAseq/eQTL/monocytes/analysis/QTL_analysis/Fastqtl/RESULTS_USED/Expression/Input_files_transcript/expression_',treatment,'.txt'),stringsAsFactors = F) # includes IFB1
  dim(expression.data)
  expression.data = expression.data[,which(colnames(expression.data) %in% colnames(TP_GENOTYPE_treatment)),with=F]
  
  identical(colnames(expression.data), colnames(TP_GENOTYPE_treatment))
  
  variance<-apply(expression.data[,-1],1,var)
  
  test.cov<-prcomp(t(expression.data[!variance==0,-1]),center=T,scale.=T)
  test.cov<-data.frame(test.cov$x[1:length(expression.data[!variance==0,-1]),1:length(expression.data[!variance==0,-1])])
  test.cov_IFN<-test.cov[,1:nPC]
}
data <- data.frame(probe=INPUT[,2], genotype=INPUT[,1], test.cov_IFN)

pcFit <- lm(data$probe ~ data$genotype + as.matrix(test.cov_IFN))
summary(pcFit)
remodelled <- data$probe - rowSums(sapply(1:nPC, function(i) pcFit$coefficients[i+2]*test.cov_IFN[,i]))
summary(INPUT[,2])
summary(remodelled)

INPUT[,2] = remodelled

#-------------

INPUT$genotype[which(INPUT$genotype == 0)] = Allele_0
INPUT$genotype[which(INPUT$genotype == 1)] = Allele_1
INPUT$genotype[which(INPUT$genotype == 2)] = Allele_2

INPUT$genotype = factor(INPUT$genotype, levels = c(Allele_0,Allele_1,Allele_2))

x <- paste("INPUT_",treatment, sep="") # INPUT_UT
eval(call("<-", as.name(x), INPUT))

#----------------- visualization

#-----------
setwd(folder)
#-----------
 

#-----------
INPUT_LPS24$treatment = rep('LPS', dim(INPUT_LPS24)[1])
INPUT_IFN$treatment = rep('IFN', dim(INPUT_IFN)[1])
INPUT_UT$treatment = rep('Naive', dim(INPUT_UT)[1])
 
data<-rbind(INPUT_LPS24, INPUT_UT, INPUT_IFN)
colnames(data)<-c("snp","gene.x","treatment") #,"stats"

data$treatment <- factor(data$treatment, levels = c('Naive','LPS','IFN'))
gene.name.1<-input$gene_name[t]
str(data)
 
library(ggplot2)
graph<-ggplot(data,aes((snp),gene.x))+geom_jitter(colour="grey35",position=position_jitter(width=0.175),alpha=0.7,size=2.8)+geom_boxplot(colour="grey15",fill="grey85",alpha=0.45,outlier.size=0)+facet_wrap(~treatment,nrow=1)+theme_bw()+ylab(gene.name.1)+xlab(input$SNP_ID)+ ggtitle(paste('tQTL', input$SNP_ID[t], '&', input$gene_name[t], sep = ' '))
#graph<-graph+geom_text(aes(x=2,y=max(gene.x-.06),label=paste("p=",stats)),size=4,colour="grey35")
print(graph)

pdf(paste(input$SNP_ID[t],input$gene_id[t],input$gene_name[t],'.pdf',sep = '_'),width = 8, height = 8, useDingbats = F)
print(graph)
dev.off()

#   }, error=function(e){})
# }
getwd()  





#------------------------ get FDR - tQTLs
library(data.table)
ifn = fread('/t1-data/data/fairfaxlab/transcriptomics/RNAseq/eQTL/monocytes/analysis/QTL_analysis/Fastqtl/RESULTS_USED/tQTL/tQTL_IFN_All.txt', stringsAsFactors = F)
lps = fread('/t1-data/data/fairfaxlab/transcriptomics/RNAseq/eQTL/monocytes/analysis/QTL_analysis/Fastqtl/RESULTS_USED/tQTL/tQTL_LPS24_All.txt', stringsAsFactors = F)
ut = fread('/t1-data/data/fairfaxlab/transcriptomics/RNAseq/eQTL/monocytes/analysis/QTL_analysis/Fastqtl/RESULTS_USED/tQTL/tQTL_UT_All.txt', stringsAsFactors = F)


INPUT = data.frame(SNP = c('rs73064425',
                           'rs9380142',
                           'rs143334143',   
                           'rs3131294',
                           'rs10735079',   
                           'rs10735079',
                           'rs2109069',
                           'rs74956615',
                           'rs74956615',
                           'rs2236757'), TRANSCRIPT = c('ZTFL1',
                                                        'HLA-G',
                                                        'CCHCR1',
                                                        'NOTCH4',
                                                        'OAS3',
                                                        'OAS1',
                                                        'DPP9',
                                                        'ICAM5',
                                                        'TYK2',
                                                        'IFNAR2'))

for(i in 1:dim(INPUT)[1])
{
  print('==========================================')
  print(INPUT$TRANSCRIPT[i])
  print('IFN')
  
  print(ifn[which(ifn$gene_name == INPUT$TRANSCRIPT[i] & ifn$SNP_ID == INPUT$SNP[i]),c('gene_name','gene_id','FDR')])
  print('lps')
  print(lps[which(lps$gene_name == INPUT$TRANSCRIPT[i] & lps$SNP_ID == INPUT$SNP[i]),c('gene_name','gene_id','FDR')])
  print('ut')
  print(ut[which(ut$gene_name == INPUT$TRANSCRIPT[i] & ut$SNP_ID == INPUT$SNP[i]),c('gene_name','gene_id','FDR')])
}


#--- eQTL
library(data.table)
ifn = fread('/t1-data/data/fairfaxlab/transcriptomics/RNAseq/eQTL/monocytes/analysis/QTL_analysis/Fastqtl/RESULTS_USED/eQTL/eQTL_IFN_All.txt', stringsAsFactors = F)
lps = fread('/t1-data/data/fairfaxlab/transcriptomics/RNAseq/eQTL/monocytes/analysis/QTL_analysis/Fastqtl/RESULTS_USED/eQTL/eQTL_LPS24_All.txt', stringsAsFactors = F)
ut = fread('/t1-data/data/fairfaxlab/transcriptomics/RNAseq/eQTL/monocytes/analysis/QTL_analysis/Fastqtl/RESULTS_USED/eQTL/eQTL_UT_All.txt', stringsAsFactors = F)

SNP = 'rs2236757'
TRANSCRIPT = 'IFNAR2'

INPUT = data.frame(SNP = c('rs73064425',
'rs9380142',
'rs143334143',   
'rs3131294',
'rs10735079',   
'rs10735079',
'rs2109069',
'rs74956615',
'rs74956615',
'rs2236757'), TRANSCRIPT = c('ZTFL1',
'HLA-G',
'CCHCR1',
'NOTCH4',
'OAS3',
'OAS1',
'DPP9',
'ICAM5',
'TYK2',
'IFNAR2'))

for(i in 1:dim(INPUT)[1])
{
  print('==========================================')
  print(INPUT$TRANSCRIPT[i])
  print('IFN')
  
print(ifn[which(ifn$gene_name == INPUT$TRANSCRIPT[i] & ifn$SNP_ID == INPUT$SNP[i]),c('gene_name','gene_id','FDR')])
print('lps')
print(lps[which(lps$gene_name == INPUT$TRANSCRIPT[i] & lps$SNP_ID == INPUT$SNP[i]),c('gene_name','gene_id','FDR')])
print('ut')
print(ut[which(ut$gene_name == INPUT$TRANSCRIPT[i] & ut$SNP_ID == INPUT$SNP[i]),c('gene_name','gene_id','FDR')])
}



#--- isoQTL
library(data.table)
ifn = fread('/t1-data/data/fairfaxlab/transcriptomics/RNAseq/eQTL/monocytes/analysis/QTL_analysis/Fastqtl/RESULTS_USED/isoQTL/isoQTL_IFN_All.txt', stringsAsFactors = F)
lps = fread('/t1-data/data/fairfaxlab/transcriptomics/RNAseq/eQTL/monocytes/analysis/QTL_analysis/Fastqtl/RESULTS_USED/isoQTL/isoQTL_LPS24_All.txt', stringsAsFactors = F)
ut = fread('/t1-data/data/fairfaxlab/transcriptomics/RNAseq/eQTL/monocytes/analysis/QTL_analysis/Fastqtl/RESULTS_USED/isoQTL/isoQTL_UT_All.txt', stringsAsFactors = F)


INPUT = data.frame(SNP = c('rs73064425',
                           'rs9380142',
                           'rs143334143',   
                           'rs3131294',
                           'rs10735079',   
                           'rs10735079',
                           'rs2109069',
                           'rs74956615',
                           'rs74956615',
                           'rs2236757'), TRANSCRIPT = c('ZTFL1',
                                                        'HLA-G',
                                                        'CCHCR1',
                                                        'NOTCH4',
                                                        'OAS3',
                                                        'OAS1',
                                                        'DPP9',
                                                        'ICAM5',
                                                        'TYK2',
                                                        'IFNAR2'))

for(i in 1:dim(INPUT)[1])
{
  print('==========================================')
  print(INPUT$TRANSCRIPT[i])
  print('IFN')
  
  print(ifn[which(ifn$gene_name == INPUT$TRANSCRIPT[i] & ifn$SNP_ID == INPUT$SNP[i]),c('gene_name','gene_id','FDR')])
  print('lps')
  print(lps[which(lps$gene_name == INPUT$TRANSCRIPT[i] & lps$SNP_ID == INPUT$SNP[i]),c('gene_name','gene_id','FDR')])
  print('ut')
  print(ut[which(ut$gene_name == INPUT$TRANSCRIPT[i] & ut$SNP_ID == INPUT$SNP[i]),c('gene_name','gene_id','FDR')])
}

