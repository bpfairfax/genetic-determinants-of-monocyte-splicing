library(dplyr)
library( ggpubr)
library(data.table)

SNPs = fread('/t1-data/data/fairfaxlab/transcriptomics/RNAseq/eQTL/monocytes/analysis/QTL_analysis/Fastqtl/transQTL/genotyping_profiles//MAF_imputed_Allsamples_revised.txt', stringsAsFactors = F)
colnames(SNPs)[3] = 'SNP_ID'

incorporate.PC = data.frame(IFN=22,LPS24=29,UT=30)

# destination folder of plots
folder = paste0('/t1-data/data/fairfaxlab/transcriptomics/RNAseq/eQTL/monocytes/analysis/QTL_analysis/Fastqtl/transQTL/USED_input_ouput/Output_RNAseq_QTLtools/Examples/')

# input = data.frame(SNP_ID='rs34288077',gene_name='CXCR6',gene_id='ENSG00000172215', stringsAsFactors = F)
input = data.frame(SNP_ID='rs10735079',gene_name='OAS1',gene_id='ENSG00000089127', stringsAsFactors = F)
# input = data.frame(SNP_ID='rs10735079',gene_name='OAS3',gene_id='ENSG00000111331', stringsAsFactors = F)

#---- print out pvalue
IFN = fread(paste0('/t1-data/data/fairfaxlab/transcriptomics/RNAseq/eQTL/monocytes/analysis/QTL_analysis/Fastqtl/transQTL/USED_input_ouput/QTLtools/IFN_metaData_cis.txt'), stringsAsFactors = F, header = F, sep = ' ')
IFN = as.data.frame(IFN)
colnames(IFN) = c('gene_ID',
 'gene_chrID',
 'gene_start',
 'gene_end',
 'strand',
 'totalcis',
 'distance',
 'index',
 'SNP_chrID',
 'SNP_position',
 'SNP_position_end',
 'pvalue',
 'slope',
 'top1')

IFN$SNP_ID = gsub('.*;','',IFN$index)

#cis_metaData = merge(cis_metaData,GTF, by = 'gene_id')
IFN$FDR = p.adjust(IFN$pvalue, method = 'fdr')

LPS24 = fread(paste0('/t1-data/data/fairfaxlab/transcriptomics/RNAseq/eQTL/monocytes/analysis/QTL_analysis/Fastqtl/transQTL/USED_input_ouput/QTLtools/LPS24_metaData_cis.txt'), stringsAsFactors = F, header = F, sep = ' ')
LPS24 = as.data.frame(LPS24)
colnames(LPS24) = c('gene_ID',
'gene_chrID',
'gene_start',
'gene_end',
'strand',
'totalcis',
'distance',
'index',
'SNP_chrID',
'SNP_position',
'SNP_position_end',
'pvalue',
'slope',
'top1')

LPS24$SNP_ID = gsub('.*;','',LPS24$index)

#cis_metaData = merge(cis_metaData,GTF, by = 'gene_id')
LPS24$FDR = p.adjust(LPS24$pvalue, method = 'fdr')

TP = input$gene_name
SNP = input$SNP_ID
IFN[which(IFN$gene_ID == TP & IFN$SNP_ID == SNP),'FDR']
LPS24[which(LPS24$gene_ID == TP & LPS24$SNP_ID == SNP),'FDR']

treatment='LPS24'
TP_id = fread(paste0('/t1-data/data/fairfaxlab/transcriptomics/RNAseq/eQTL/monocytes/analysis/QTL_analysis/Fastqtl/transQTL/USED_input_ouput/QTLtools/expression_',treatment,'_metaData.txt'),select='id',stringsAsFactors = F) # includes IFB1
input = input[which(input$gene_name %in% TP_id$id),]
dim(input)
 
library(dplyr)
input = left_join(input, SNPs, by = 'SNP_ID')
t=1
PC = T
 
SNP = input$SNP_ID[t]
TRANSCRIPT = input$gene_id[t]
GENE = input$gene_name[t]

print(GENE)

Allele_0 = paste0(input$REF[t], input$REF[t])
Allele_1 = paste0(input$REF[t], input$ALT[t])
Allele_2 = paste0(input$ALT[t], input$ALT[t])
 
ListSNP = fread('/t1-data/data/fairfaxlab/transcriptomics/RNAseq/eQTL/monocytes/analysis/QTL_analysis/Fastqtl/transQTL/USED_input_ouput/QTLtools/Genotypes_metaData/list_SNPs.txt',header = T, stringsAsFactors = F)
head(ListSNP)

#------------------------------------------- IFN
treatment='IFN'
library(data.table)
setwd('/t1-data/data/fairfaxlab/transcriptomics/RNAseq/eQTL/monocytes/analysis/QTL_analysis/Fastqtl/transQTL/USED_input_ouput/QTLtools/')
TP_EXPESSION = fread(paste0('expression_',treatment,'_metaData.txt'),skip = GENE, nrows = 1,stringsAsFactors = F) # includes IFB1
EXPESSION_sample_names = fread(paste0('expression_',treatment,'_metaData.txt'),nrows = 1,stringsAsFactors = F) # includes IFB1
colnames(TP_EXPESSION) = colnames(EXPESSION_sample_names)

setwd(paste0('/t1-data/data/fairfaxlab/transcriptomics/RNAseq/eQTL/monocytes/analysis/QTL_analysis/Fastqtl/transQTL/USED_input_ouput/QTLtools/Genotypes_metaData/',treatment))
GENOTYPE = fread(paste0(treatment,'_metaData_012.012'),select=(which(ListSNP$ID == SNP)+1), stringsAsFactors = F)
TP_GENOTYPE = t(GENOTYPE)

GENOTYPE_sample_names = fread(paste0(treatment,'_metaData_012.012.indv'), stringsAsFactors = F,header = F)
colnames(TP_GENOTYPE) = GENOTYPE_sample_names$V1
TP_GENOTYPE = data.frame(ID = SNP, TP_GENOTYPE)
row.names(TP_GENOTYPE) = NULL
colnames(TP_GENOTYPE) = gsub('X','',colnames(TP_GENOTYPE))
 
TP_EXPESSION = TP_EXPESSION[,which(colnames(TP_EXPESSION) %in% colnames(TP_GENOTYPE)),with=F]
TP_GENOTYPE_treatment = TP_GENOTYPE[,which(colnames(TP_GENOTYPE) %in% colnames(TP_EXPESSION))]
dim(TP_GENOTYPE_treatment)
dim(TP_EXPESSION)

identical(colnames(TP_EXPESSION) , colnames(TP_GENOTYPE_treatment))

INPUT = rbind(as.matrix(TP_GENOTYPE_treatment), TP_EXPESSION)
INPUT = t(INPUT)
dim(INPUT)
colnames(INPUT) = c('genotype',GENE)
INPUT = as.data.frame(INPUT)

INPUT = INPUT[-1,]

INPUT[,1] = as.numeric(as.character(INPUT[,1]))
table(INPUT[,1])
INPUT[,2] = as.numeric(as.character(INPUT[,2]))

#INPUT[,2] = INPUT[,2]+10
summary(INPUT[,2])
#------------- regress out

message("Calculating PC")
nPC = incorporate.PC[,(colnames(incorporate.PC) == treatment)]

if(t==1 & PC)
{
setwd('/t1-data/data/fairfaxlab/transcriptomics/RNAseq/eQTL/monocytes/analysis/QTL_analysis/Fastqtl/transQTL/USED_input_ouput/QTLtools/')
expression.data = fread(paste0('expression_',treatment,'_metaData.txt'),stringsAsFactors = F) # includes IFB1
EXPESSION_sample_names = fread(paste0('expression_',treatment,'_metaData.txt'),nrows = 1,stringsAsFactors = F) # includes IFB1
colnames(expression.data) = colnames(EXPESSION_sample_names)
expression.data = expression.data[,which(colnames(expression.data) %in% colnames(TP_GENOTYPE)),with=F]

identical(colnames(expression.data), colnames(TP_GENOTYPE_treatment))
dim(expression.data)
variance<-apply(expression.data[,-1],1,var)

test.cov<-prcomp(t(expression.data[!variance==0,-1]),center=T,scale.=T)
test.cov<-data.frame(test.cov$x[1:length(expression.data[!variance==0,-1]),1:length(expression.data[!variance==0,-1])])
test.cov<-test.cov[,1:nPC]
}

data <- data.frame(probe=INPUT[,2], genotype=INPUT[,1], test.cov)

pcFit <- lm(data$probe ~ data$genotype + as.matrix(test.cov))
summary(pcFit)
remodelled <- data$probe - rowSums(sapply(1:nPC, function(i) pcFit$coefficients[i+2]*test.cov[,i]))
summary(remodelled)

# cor(remodelledd10, remodelledm10)
# summary(INPUT[,2]/10)
# summary(remodelled/10)

INPUT[,2] = remodelled
table(INPUT$genotype)
#-------------

INPUT$genotype[which(INPUT$genotype == 0)] = Allele_0
INPUT$genotype[which(INPUT$genotype == 1)] = Allele_1
INPUT$genotype[which(INPUT$genotype == 2)] = Allele_2

INPUT$genotype = factor(INPUT$genotype, levels = c(Allele_0,Allele_1,Allele_2))


x <- paste("INPUT_",treatment, sep="") # INPUT_UT
eval(call("<-", as.name(x), INPUT))
 

#------------------------------------------- LPS
treatment='LPS24'
library(data.table)
setwd('/t1-data/data/fairfaxlab/transcriptomics/RNAseq/eQTL/monocytes/analysis/QTL_analysis/Fastqtl/transQTL/USED_input_ouput/QTLtools/')
TP_EXPESSION = fread(paste0('expression_',treatment,'_metaData.txt'),skip = GENE, nrows = 1,stringsAsFactors = F) # includes IFB1
EXPESSION_sample_names = fread(paste0('expression_',treatment,'_metaData.txt'),nrows = 1,stringsAsFactors = F) # includes IFB1
colnames(TP_EXPESSION) = colnames(EXPESSION_sample_names)

setwd(paste0('/t1-data/data/fairfaxlab/transcriptomics/RNAseq/eQTL/monocytes/analysis/QTL_analysis/Fastqtl/transQTL/USED_input_ouput/QTLtools/Genotypes_metaData/',treatment))
GENOTYPE = fread(paste0(treatment,'_metaData_012.012'),select=(which(ListSNP$ID == SNP)+1), stringsAsFactors = F)
TP_GENOTYPE = t(GENOTYPE)

GENOTYPE_sample_names = fread(paste0(treatment,'_metaData_012.012.indv'), stringsAsFactors = F,header = F)
colnames(TP_GENOTYPE) = GENOTYPE_sample_names$V1
TP_GENOTYPE = data.frame(ID = SNP, TP_GENOTYPE)
row.names(TP_GENOTYPE) = NULL
colnames(TP_GENOTYPE) = gsub('X','',colnames(TP_GENOTYPE))

TP_EXPESSION = TP_EXPESSION[,which(colnames(TP_EXPESSION) %in% colnames(TP_GENOTYPE)),with=F]
TP_GENOTYPE_treatment = TP_GENOTYPE[,which(colnames(TP_GENOTYPE) %in% colnames(TP_EXPESSION))]
dim(TP_GENOTYPE_treatment)
dim(TP_EXPESSION)

identical(colnames(TP_EXPESSION) , colnames(TP_GENOTYPE_treatment))

INPUT = rbind(as.matrix(TP_GENOTYPE_treatment), TP_EXPESSION)
INPUT = t(INPUT)
dim(INPUT)
colnames(INPUT) = c('genotype',GENE)
INPUT = as.data.frame(INPUT)

INPUT = INPUT[-1,]

INPUT[,1] = as.numeric(as.character(INPUT[,1]))
table(INPUT[,1])
INPUT[,2] = as.numeric(as.character(INPUT[,2]))

#INPUT[,2] = INPUT[,2]+10
summary(INPUT[,2])
#------------- regress out

message("Calculating PC")
nPC = incorporate.PC[,(colnames(incorporate.PC) == treatment)]

if(t==1 & PC)
{
setwd('/t1-data/data/fairfaxlab/transcriptomics/RNAseq/eQTL/monocytes/analysis/QTL_analysis/Fastqtl/transQTL/USED_input_ouput/QTLtools/')
expression.data = fread(paste0('expression_',treatment,'_metaData.txt'),stringsAsFactors = F) # includes IFB1
EXPESSION_sample_names = fread(paste0('expression_',treatment,'_metaData.txt'),nrows = 1,stringsAsFactors = F) # includes IFB1
colnames(expression.data) = colnames(EXPESSION_sample_names)
expression.data = expression.data[,which(colnames(expression.data) %in% colnames(TP_GENOTYPE)),with=F]

identical(colnames(expression.data), colnames(TP_GENOTYPE_treatment))
dim(expression.data)
variance<-apply(expression.data[,-1],1,var)

test.cov<-prcomp(t(expression.data[!variance==0,-1]),center=T,scale.=T)
test.cov<-data.frame(test.cov$x[1:length(expression.data[!variance==0,-1]),1:length(expression.data[!variance==0,-1])])
test.cov<-test.cov[,1:nPC]
}

data <- data.frame(probe=INPUT[,2], genotype=INPUT[,1], test.cov)

pcFit <- lm(data$probe ~ data$genotype + as.matrix(test.cov))
summary(pcFit)
remodelled <- data$probe - rowSums(sapply(1:nPC, function(i) pcFit$coefficients[i+2]*test.cov[,i]))
summary(remodelled)

# cor(remodelledd10, remodelledm10)
# summary(INPUT[,2]/10)
# summary(remodelled/10)

INPUT[,2] = remodelled
table(INPUT$genotype)
#-------------

INPUT$genotype[which(INPUT$genotype == 0)] = Allele_0
INPUT$genotype[which(INPUT$genotype == 1)] = Allele_1
INPUT$genotype[which(INPUT$genotype == 2)] = Allele_2

INPUT$genotype = factor(INPUT$genotype, levels = c(Allele_0,Allele_1,Allele_2))


x <- paste("INPUT_",treatment, sep="") # INPUT_UT
eval(call("<-", as.name(x), INPUT))


#------------------------------------------- UT
treatment='UT'
library(data.table)
setwd('/t1-data/data/fairfaxlab/transcriptomics/RNAseq/eQTL/monocytes/analysis/QTL_analysis/Fastqtl/transQTL/USED_input_ouput/QTLtools/')
TP_EXPESSION = fread(paste0('expression_',treatment,'_metaData.txt'),skip = GENE, nrows = 1,stringsAsFactors = F) # includes IFB1
EXPESSION_sample_names = fread(paste0('expression_',treatment,'_metaData.txt'),nrows = 1,stringsAsFactors = F) # includes IFB1
colnames(TP_EXPESSION) = colnames(EXPESSION_sample_names)

setwd(paste0('/t1-data/data/fairfaxlab/transcriptomics/RNAseq/eQTL/monocytes/analysis/QTL_analysis/Fastqtl/transQTL/USED_input_ouput/QTLtools/Genotypes_metaData/',treatment))
GENOTYPE = fread(paste0(treatment,'_metaData_012.012'),select=(which(ListSNP$ID == SNP)+1), stringsAsFactors = F)
TP_GENOTYPE = t(GENOTYPE)

GENOTYPE_sample_names = fread(paste0(treatment,'_metaData_012.012.indv'), stringsAsFactors = F,header = F)
colnames(TP_GENOTYPE) = GENOTYPE_sample_names$V1
TP_GENOTYPE = data.frame(ID = SNP, TP_GENOTYPE)
row.names(TP_GENOTYPE) = NULL
colnames(TP_GENOTYPE) = gsub('X','',colnames(TP_GENOTYPE))

TP_EXPESSION = TP_EXPESSION[,which(colnames(TP_EXPESSION) %in% colnames(TP_GENOTYPE)),with=F]
TP_GENOTYPE_treatment = TP_GENOTYPE[,which(colnames(TP_GENOTYPE) %in% colnames(TP_EXPESSION))]
dim(TP_GENOTYPE_treatment)
dim(TP_EXPESSION)

identical(colnames(TP_EXPESSION) , colnames(TP_GENOTYPE_treatment))

INPUT = rbind(as.matrix(TP_GENOTYPE_treatment), TP_EXPESSION)
INPUT = t(INPUT)
dim(INPUT)
colnames(INPUT) = c('genotype',GENE)
INPUT = as.data.frame(INPUT)

INPUT = INPUT[-1,]

INPUT[,1] = as.numeric(as.character(INPUT[,1]))
table(INPUT[,1])
INPUT[,2] = as.numeric(as.character(INPUT[,2]))

#INPUT[,2] = INPUT[,2]+10
summary(INPUT[,2])
#------------- regress out

message("Calculating PC")
nPC = incorporate.PC[,(colnames(incorporate.PC) == treatment)]

if(t==1 & PC)
{
  setwd('/t1-data/data/fairfaxlab/transcriptomics/RNAseq/eQTL/monocytes/analysis/QTL_analysis/Fastqtl/transQTL/USED_input_ouput/QTLtools/')
  expression.data = fread(paste0('expression_',treatment,'_metaData.txt'),stringsAsFactors = F) # includes IFB1
  EXPESSION_sample_names = fread(paste0('expression_',treatment,'_metaData.txt'),nrows = 1,stringsAsFactors = F) # includes IFB1
  colnames(expression.data) = colnames(EXPESSION_sample_names)
  expression.data = expression.data[,which(colnames(expression.data) %in% colnames(TP_GENOTYPE)),with=F]
  
  identical(colnames(expression.data), colnames(TP_GENOTYPE_treatment))
  dim(expression.data)
  variance<-apply(expression.data[,-1],1,var)
  
  test.cov<-prcomp(t(expression.data[!variance==0,-1]),center=T,scale.=T)
  test.cov<-data.frame(test.cov$x[1:length(expression.data[!variance==0,-1]),1:length(expression.data[!variance==0,-1])])
  test.cov<-test.cov[,1:nPC]
}

data <- data.frame(probe=INPUT[,2], genotype=INPUT[,1], test.cov)

pcFit <- lm(data$probe ~ data$genotype + as.matrix(test.cov))
summary(pcFit)
remodelled <- data$probe - rowSums(sapply(1:nPC, function(i) pcFit$coefficients[i+2]*test.cov[,i]))
summary(remodelled)

# cor(remodelledd10, remodelledm10)
# summary(INPUT[,2]/10)
# summary(remodelled/10)

INPUT[,2] = remodelled
table(INPUT$genotype)
#-------------

INPUT$genotype[which(INPUT$genotype == 0)] = Allele_0
INPUT$genotype[which(INPUT$genotype == 1)] = Allele_1
INPUT$genotype[which(INPUT$genotype == 2)] = Allele_2

INPUT$genotype = factor(INPUT$genotype, levels = c(Allele_0,Allele_1,Allele_2))


x <- paste("INPUT_",treatment, sep="") # INPUT_UT
eval(call("<-", as.name(x), INPUT))


#----------------- visualization

#-----------
setwd(folder)
#-----------
 
#-----------
INPUT_LPS24$treatment = rep('LPS', dim(INPUT_LPS24)[1])
INPUT_IFN$treatment = rep('IFN', dim(INPUT_IFN)[1])
INPUT_UT$treatment = rep('Naive', dim(INPUT_UT)[1])
 
data<-rbind(INPUT_UT, INPUT_LPS24, INPUT_IFN)
colnames(data)<-c("snp","gene.x","treatment") #,"stats"

data$treatment <- factor(data$treatment, levels = c('Naive', 'LPS','IFN'))
gene.name.1<-input$gene_name[t]
str(data)

#data$snp = factor(data$snp, levels =(names(table(data$snp))))
#data = data[order(data$snp, decreasing = T),]

library(ggplot2)
graph<-ggplot(data,aes((snp),gene.x))+geom_jitter(colour="grey35",position=position_jitter(width=0.175),alpha=0.7,size=2.8)+geom_boxplot(colour="grey15",fill="grey85",alpha=0.45,outlier.size=0)+facet_wrap(~treatment,nrow=1)+theme_bw()+ylab(gene.name.1)+xlab(input$SNP_ID)+ ggtitle(paste('eQTL', input$SNP_ID[t], '&', input$gene_name[t], sep = ' '))
#graph<-graph+geom_text(aes(x=2,y=max(gene.x-.06),label=paste("p=",stats)),size=4,colour="grey35")
print(graph)

pdf(paste(input$SNP_ID[t],input$gene_id[t],input$gene_name[t],'_threeTreatment_metaData.pdf',sep = '_'),width = 8, height = 8, useDingbats = F)
print(graph)
dev.off()

# }, error=function(e){})
# }
getwd()



