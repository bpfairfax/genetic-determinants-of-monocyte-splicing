library(dplyr)
library( ggpubr)
## save and load
# setwd('/Users/isar.nassiri/Desktop/Analysis_monocyte/Results/wiggleplot/')
# transcript_metadata = readRDS('transcript_metadata.rds')
# selected_transcripts = transcript_metadata %>%
#   dplyr::filter(gene_name == "PSMB9", transcript_biotype == "protein_coding")
# tx_ids = selected_transcripts$transcript_id

SNP = 'rs10735079'
TRANSCRIPT = 'ENST00000202917'
GENE = 'OAS1'

Allele_0 = 'GG'
Allele_1 = 'GA'
Allele_2 = 'AA'
 
#------------------------------------------- IFN
 
library(data.table)
TP_EXPESSION = fread('/Users/isar.nassiri/Desktop/Analysis_monocyte/Results/ARTICLE/Input/IU/expression_IFN_IU.txt',stringsAsFactors = F)
colnames(TP_EXPESSION)[1] = 'id'
colnames(TP_EXPESSION) = sub('.*_','',colnames(TP_EXPESSION))

TP_GENOTYPE = fread('/Volumes/Isar_Nassiri_Fairfax_lab/RESULTS_USED/Genotype/Monocyte_imputed_matrixQTL_IFN_justSNPs_format2_septab.txt',stringsAsFactors = F)
colnames(TP_GENOTYPE) = sub('.*_','',colnames(TP_GENOTYPE))

TP_EXPESSION = TP_EXPESSION[,which(colnames(TP_EXPESSION) %in% colnames(TP_GENOTYPE)),with = F]
TP_GENOTYPE = TP_GENOTYPE[,(colnames(TP_GENOTYPE) %in% colnames(TP_EXPESSION)),with=F]

TP_EXPESSION_subset = subset(TP_EXPESSION, TP_EXPESSION$id %in% TRANSCRIPT)
dim(TP_EXPESSION_subset)

TP_GENOTYPE_sub = subset(TP_GENOTYPE, TP_GENOTYPE$id == SNP)

dim(TP_EXPESSION_subset)
dim(TP_GENOTYPE_sub)

INPUT = rbind(TP_GENOTYPE_sub[,-1], TP_EXPESSION_subset[,-c(1)], fill = T)
INPUT = t(INPUT)
dim(INPUT)
colnames(INPUT) = c('genotype',GENE)
INPUT = as.data.frame(INPUT)

INPUT$genotype[which(INPUT$genotype == 0)] = Allele_0
INPUT$genotype[which(INPUT$genotype == 1)] = Allele_1
INPUT$genotype[which(INPUT$genotype == 2)] = Allele_2

INPUT$genotype = factor(INPUT$genotype, levels = c(Allele_0,Allele_1,Allele_2))
INPUT_IFN = INPUT
# setwd('/Users/isar.nassiri/Desktop/')
# for(h in 1:length(GENE))
# {
#   p = ggviolin(INPUT, x = "genotype",
#                y = GENE[h],
#                title = paste('tQTL:', TP_IDS[h],'-',TP_IDS2[h],'&','rs1580345'),
#                combine = TRUE, 
#                color = "genotype", palette = "jco",
#                ylab = "Expression", 
#                add = "boxplot")
#   print(p)
#   # ggarrange(p, ncol = 1) %>%
#   #   ggexport(filename = paste0(TP_IDS[h],'.IFN.PSMB9.png'), res = 300)
# }
 
# setwd('/Users/isar.nassiri/Desktop')
# tiff(paste0('tQTL.IFN.',TP_IDS[h],'.',TP_IDS2[h],'.IFN.tiff'), units="in", width=5, height=5, res=300)
# print(p)
# dev.off()

#------------------------------------------- UT
rm(list=setdiff(ls(), c("INPUT_UT",'SNP','TRANSCRIPT','GENE','Allele_0','Allele_1','Allele_2',"INPUT_IFN")))
library(data.table)
TP_EXPESSION = fread('/Users/isar.nassiri/Desktop/Analysis_monocyte/Results/ARTICLE/Input/IU/expression_UT_IU.txt',stringsAsFactors = F)
colnames(TP_EXPESSION)[1] = 'id'
colnames(TP_EXPESSION) = sub('.*_','',colnames(TP_EXPESSION))

TP_GENOTYPE = fread('/Volumes/Isar_Nassiri_Fairfax_lab/RESULTS_USED/Genotype/Monocyte_imputed_matrixQTL_UT_justSNPs_format2_septab.txt',stringsAsFactors = F)
colnames(TP_GENOTYPE) = sub('.*_','',colnames(TP_GENOTYPE))

TP_EXPESSION = TP_EXPESSION[,which(colnames(TP_EXPESSION) %in% colnames(TP_GENOTYPE)),with=F]
TP_GENOTYPE = TP_GENOTYPE[,(colnames(TP_GENOTYPE) %in% colnames(TP_EXPESSION)),with=F]

TP_EXPESSION_subset = subset(TP_EXPESSION, TP_EXPESSION$id %in% TRANSCRIPT)
dim(TP_EXPESSION_subset) 

TP_GENOTYPE_sub = subset(TP_GENOTYPE, TP_GENOTYPE$id == SNP)

dim(TP_EXPESSION_subset)
dim(TP_GENOTYPE_sub)

INPUT = rbind(TP_GENOTYPE_sub[,-1], TP_EXPESSION_subset[,-c(1)], fill = T)
INPUT = t(INPUT)
dim(INPUT)
colnames(INPUT) = c('genotype',GENE)
INPUT = as.data.frame(INPUT)

INPUT$genotype[which(INPUT$genotype == 0)] = Allele_0
INPUT$genotype[which(INPUT$genotype == 1)] = Allele_1
INPUT$genotype[which(INPUT$genotype == 2)] = Allele_2

INPUT$genotype = factor(INPUT$genotype, levels = c(Allele_0,Allele_1,Allele_2))
INPUT_UT = INPUT

# setwd('/Users/isar.nassiri/Desktop/')
# for(h in 1:length(TP_IDS))
# {
#   p = ggviolin(INPUT, x = "genotype",
#                y = TP_IDS[h],
#                title = paste('tQTL:', TP_IDS[h],'-',TP_IDS2[h],'&','rs1580345'),
#                combine = TRUE, 
#                color = "genotype", palette = "jco",
#                ylab = "Expression", 
#                add = "boxplot")
#   print(p)
#   # ggarrange(p, ncol = 1) %>%
#   #   ggexport(filename = paste0(TP_IDS[h],'.IFN.PSMB9.png'), res = 300)
# }
# setwd('/Users/isar.nassiri/Desktop')
# tiff(paste0('tQTL.UT.',TP_IDS[h],'.',TP_IDS2[h],'.IFN.tiff'), units="in", width=5, height=5, res=300)
# print(p)
# dev.off()


#------------------------------------------- LPS24
rm(list=setdiff(ls(), c("INPUT_UT",'SNP','TRANSCRIPT','GENE','Allele_0','Allele_1','Allele_2',"INPUT_IFN", "INPUT_UT")))
library(data.table)
TP_EXPESSION = fread('/Users/isar.nassiri/Desktop/Analysis_monocyte/Results/ARTICLE/Input/IU/expression_LPS24_IU.txt',stringsAsFactors = F)
colnames(TP_EXPESSION)[1] = 'id'
colnames(TP_EXPESSION) = sub('.*_','',colnames(TP_EXPESSION))

TP_GENOTYPE = fread('/Volumes/Isar_Nassiri_Fairfax_lab/RESULTS_USED/Genotype/Monocyte_imputed_matrixQTL_LPS24_justSNPs_format2_septab.txt',stringsAsFactors = F)
colnames(TP_GENOTYPE) = sub('.*_','',colnames(TP_GENOTYPE))

TP_EXPESSION = TP_EXPESSION[,which(colnames(TP_EXPESSION) %in% colnames(TP_GENOTYPE)),with=F]
TP_GENOTYPE = TP_GENOTYPE[,(colnames(TP_GENOTYPE) %in% colnames(TP_EXPESSION)),with=F]

TP_EXPESSION_subset = subset(TP_EXPESSION, TP_EXPESSION$id %in% TRANSCRIPT)
dim(TP_EXPESSION_subset)

TP_GENOTYPE_sub = subset(TP_GENOTYPE, TP_GENOTYPE$id == SNP)

dim(TP_EXPESSION_subset)
dim(TP_GENOTYPE_sub)

INPUT = rbind(TP_GENOTYPE_sub[,-1], TP_EXPESSION_subset[,-c(1)], fill = T)
INPUT = t(INPUT)
dim(INPUT)
colnames(INPUT) = c('genotype',GENE)
INPUT = as.data.frame(INPUT)

INPUT$genotype[which(INPUT$genotype == 0)] = Allele_0
INPUT$genotype[which(INPUT$genotype == 1)] = Allele_1
INPUT$genotype[which(INPUT$genotype == 2)] = Allele_2

INPUT$genotype = factor(INPUT$genotype, levels = c(Allele_0,Allele_1,Allele_2))
INPUT_LPS24 = INPUT

#----------------- visualization

# just one treatment
# p = ggboxplot(INPUT_IFN, x = "genotype",
#               y = GENE,
#               combine = TRUE,
#               color = 'genotype', palette = c('#f3ac38','#f3ac38','#f3ac38'),
#               ylab = "Expression (TPM)", 
#               add = "jitter",                              # Add jittered points
#               add.params = list(size = 0.1, jitter = 0.2)
# )
#----------------- IFN and UT

INPUT_LPS24$treatment = rep('LPS', dim(INPUT_LPS24)[1])
INPUT_IFN$treatment = rep('IFN', dim(INPUT_IFN)[1])
INPUT_UT$treatment = rep('Naive', dim(INPUT_UT)[1])

setwd('/Users/isar.nassiri/Desktop')
library(ggpubr)
library(ggplot2)

data<-rbind(INPUT_UT, INPUT_IFN)
colnames(data)<-c("snp","gene.x","treatment") #,"stats"

data$treatment <- factor(data$treatment, levels = c('Naive','IFN'))
gene.name.1<-GENE
str(data)

#data$snp = factor(data$snp, levels =(names(table(data$snp))))
#data = data[order(data$snp, decreasing = T),]

library(ggplot2)
graph<-ggplot(data,aes((snp),gene.x))+geom_jitter(colour="grey35",position=position_jitter(width=0.175),alpha=0.7,size=2.8)+geom_boxplot(colour="grey15",fill="grey85",alpha=0.45,outlier.size=0)+facet_wrap(~treatment,nrow=1)+theme_bw()+ylab(gene.name.1)+xlab(SNP)+ ggtitle(paste('eQTL', SNP, '&', GENE, sep = ' '))
#graph<-graph+geom_text(aes(x=2,y=max(gene.x-.06),label=paste("p=",stats)),size=4,colour="grey35")
print(graph)

pdf(paste(SNP, TRANSCRIPT, GENE,'_isoQTL.pdf',sep = '_'),width = 8, height = 8, useDingbats = F)
print(graph)
dev.off()




 




