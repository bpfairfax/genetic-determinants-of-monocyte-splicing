
#################################### Enrichment using clusterProfiler ###############################
library(qusage)
gmtfile <- c("/h.all.v7.0.symbols.gmt")  #h.all.v7.0.symbols.gmt
c5 <- read.gmt(gmtfile)
library(clusterProfiler)
egmt <- enricher(input, TERM2GENE=c5)

library(cowplot)
p2 <- dotplot(egmt, showCategory=25) + ggtitle("Trans eQTL Pathway Enrichment")
pdf(paste0('/Enrichment_tQTL_CS_All.pdf'),width = 10, height = 10,useDingbats = FALSE)
print(plot_grid(p2, ncol=1))
dev.off()

p1 <- cnetplot(egmt, foldChange=NULL, showCategory = 5)
p3 <- cnetplot(egmt, foldChange=NULL, circular = TRUE, colorEdge = TRUE, showCategory = 5)

#################################### Enrichment using XGR ###############################
library(XGR)
input = fread('/TP_ALL_LPS24_SNPsPositions_on_Regul.Regions_CSG.txt', stringsAsFactors = F)

Canonical_pathways <- xEnricherGenes(data=input$gene_name, ontology="REACTOME")
# oncogenic_signatures <- xEnricherGenes(data=DEG_CT$gene_name, ontology='MsigdbC6')

Canonical_pathways_concise <- xEnrichConciser(Canonical_pathways)
res <- xEnrichViewer(Canonical_pathways_concise, top_num=length(Canonical_pathways_concise$adjp), sortBy="adjp", details=TRUE)
output <- data.frame(term=rownames(res), res)

output_subset = output#[which(output$namespace == 'Metabolism'),]
dim(output_subset)
output_subset = output_subset[, c("name", "namespace", "nAnno", "nOverlap", "zscore", "pvalue", "adjp", "members")]
output_subset$GeneRatio = paste0(output_subset$nOverlap,'/',output_subset$nAnno)
output_subset$Count = output_subset$nOverlap
output_subset$BgRatio = paste0(output_subset$nOverlap,'/10718')
output_subset = output_subset[,c("name", "name", "GeneRatio", "BgRatio", "pvalue", "adjp", "adjp",  "members", 'Count')]
colnames(output_subset) = c("ID","Description","GeneRatio","BgRatio","pvalue","p.adjust","qvalue","geneID", 'Count')
output_subset$geneID = gsub(', ','/', output_subset$geneID)
row.names(output_subset) = output_subset$ID
dim(output_subset)

row.names(output_subset)[3] = "Respiratory electron transport."
output_subset$ID[3] = "Respiratory electron transport."
output_subset$Description[3] = "Respiratory electron transport."

library(cowplot)

#################################### Enrichment using DOSE ###############################
library(DOSE)
data(geneList)
de <- names(geneList)[1:100]
x <- enrichDO(de)
#-----

x@result = output_subset
setwd('/folder/')
pdf('Metformin_dotplot.pdf', width = 10, height = 10)
dotplot(x, showCategory=30)
dev.off()

#################################### Enrichment using ReactomePA ###############################
library(ReactomePA)
library('org.Hs.eg.db')
keytypes(org.Hs.eg.db)

input = fread('/TP_ALL_LPS24_SNPsPositions_on_Regul.Regions_CSG.txt', stringsAsFactors = F)
de = as.character(mapIds(org.Hs.eg.db, input$gene_name, 'ENTREZID','SYMBOL'))

# gene <- seq2gene(peak, tssRegion = c(-1000, 1000), flankDistance = 3000, TxDb=txdb)
# length(unique(gene))
pathway2 <- enrichPathway(de, organism = "human")
head(pathway2, 2)

pdf("Pathways_dotplot.pdf",width=10,height=10,useDingbats = FALSE)
dotplot(pathway2)
dev.off()


