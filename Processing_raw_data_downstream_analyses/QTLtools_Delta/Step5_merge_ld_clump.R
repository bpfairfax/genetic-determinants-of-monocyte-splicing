
#------------------- merge the results

library(data.table)
listfiles = list.files(paste0('/t1-data/data/fairfaxlab/transcriptomics/RNAseq/eQTL/monocytes/analysis/QTL_analysis/Fastqtl/deltaQTL_QTLtools/nominal_chunks_imputedGenotype/ld_clump/'), pattern = '.txt')
length(listfiles)
# sizes <- file.info(listfiles)$size
# # subset the files that have non-zero size
# list.of.non.empty.files <- listfiles[which(sizes != 1)]
setwd('/t1-data/data/fairfaxlab/transcriptomics/RNAseq/eQTL/monocytes/analysis/QTL_analysis/Fastqtl/deltaQTL_QTLtools/nominal_chunks_imputedGenotype/ld_clump/')
tables <- lapply(listfiles, fread, header = TRUE)
MERGED = do.call(rbind, tables)

MERGED[which(MERGED$CpG == 'cg17462560'),]
write.table(MERGED,paste0('/t1-data/data/fairfaxlab/transcriptomics/RNAseq/eQTL/monocytes/analysis/QTL_analysis/Fastqtl/deltaQTL_QTLtools/nominal_chunks_imputedGenotype/Peak_SNPs_LD_deltaQT.txt'), quote = F, row.names = F, sep = '\t')
