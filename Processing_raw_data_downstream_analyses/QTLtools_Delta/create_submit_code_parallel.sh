
DIR=/t1-data/data/fairfaxlab/transcriptomics/RNAseq/eQTL/monocytes/analysis/QTL_analysis/Fastqtl/deltaQTL_QTLtools/nominal_chunks_imputedGenotype/ld_clump/
mkdir $DIR'/script/'
cd $DIR'/script/'

for j in {1..83341}
do
cp -fr Step4_ld_clump.R 'Step4_ld_clump_'$j'.R' -f
# Replace line 11
sed -i '12s/i=1/i='$j'/' 'Step4_ld_clump_'$j'.R'
done


#> length(Num_genes)
#[1] 47214

rm -f Step4_ld_clump.R

# Creat .sh script
for f in $(find -type f -name '*.R'); do
NAME=$(echo $f | awk -F/ '{print $NF}')

echo -e '#!/bin/sh
#$ -cwd
#$ -q batchq
module add R-cbrg
cd '$DIR'/script/
R --vanilla < '${f}' > '${f}'.txt' > ${f}'.sh'
sbatch $NAME'.sh'
done


 


 