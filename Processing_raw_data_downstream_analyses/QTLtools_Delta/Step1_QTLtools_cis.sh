 
#--------------------------------- cis deltaQTL - QTLtools - arraySNPs
#-- PC selection --

#--- create matrix of covariance
cd /t1-data/data/fairfaxlab/transcriptomics/RNAseq/eQTL/monocytes/analysis/QTL_analysis/Fastqtl/deltaQTL_QTLtools/
mkdir covariance

for i in {2..51}  # i=2 => 1 PC
do

head -$i '/t1-data/data/fairfaxlab/transcriptomics/RNAseq/eQTL/monocytes/analysis/QTL_analysis/Fastqtl/deltaQTL_QTLtools/covariance_methylation.txt' > '/t1-data/data/fairfaxlab/transcriptomics/RNAseq/eQTL/monocytes/analysis/QTL_analysis/Fastqtl/deltaQTL_QTLtools/covariance/covariance_methylation_'$i'.txt'
done


#--- creat index files
module add samtools 
bgzip -i /t1-data/data/fairfaxlab/transcriptomics/RNAseq/eQTL/monocytes/analysis/QTL_analysis/Fastqtl/deltaQTL_QTLtools/Delta_profile.bed && tabix -p bed /t1-data/data/fairfaxlab/transcriptomics/RNAseq/eQTL/monocytes/analysis/QTL_analysis/Fastqtl/deltaQTL_QTLtools/Delta_profile.bed.gz

#--- QTLtools

cd /t1-data/data/fairfaxlab/transcriptomics/RNAseq/eQTL/monocytes/analysis/QTL_analysis/Fastqtl/deltaQTL_QTLtools/

for i in {2..51}
do
echo '#!/bin/sh
#$ -cwd
#$ -q batchq
cd /t1-data/data/fairfaxlab/transcriptomics/RNAseq/eQTL/monocytes/analysis/QTL_analysis/Fastqtl/deltaQTL_QTLtools/
module load qtltools
QTLtools cis --vcf /t1-data/data/fairfaxlab/transcriptomics/RNAseq/eQTL/monocytes/analysis/QTL_analysis/Fastqtl/transQTL/USED_input_ouput/QTLtools/Genotypes_metaData/LPS24_arraySNPs/out.recode.vcf.gz --bed Delta_profile.bed.gz --cov covariance/covariance_methylation_'$i'.txt --permute 1000 --out PC_Selection/QTL_'$i'.txt --window 1000000 --seed 123456' > script/Script_$i.sh
done

#------------------- submit jobs
 
cd /t1-data/data/fairfaxlab/transcriptomics/RNAseq/eQTL/monocytes/analysis/QTL_analysis/Fastqtl/deltaQTL_QTLtools/script

for line in $(find -type f -name '*.sh'); do
sbatch $line
done

squeue -u nassisar

#-------------------



#============================ imputed Genotype - premutation
#--- creat index files
module add samtools 
tabix -p vcf /t1-data/data/fairfaxlab/transcriptomics/RNAseq/eQTL/monocytes/analysis/QTL_analysis/Fastqtl/transQTL/USED_input_ouput/QTLtools/Genotypes_metaData/LPS24/out.recode.vcf.gz

cd /t1-data/data/fairfaxlab/transcriptomics/RNAseq/eQTL/monocytes/analysis/QTL_analysis/Fastqtl/deltaQTL_QTLtools/
# Run the conditional analysis
echo '#!/bin/sh
#$ -cwd
#$ -q batchq
module load qtltools
cd /t1-data/data/fairfaxlab/transcriptomics/RNAseq/eQTL/monocytes/analysis/QTL_analysis/Fastqtl/deltaQTL_QTLtools/
QTLtools cis --vcf /t1-data/data/fairfaxlab/transcriptomics/RNAseq/eQTL/monocytes/analysis/QTL_analysis/Fastqtl/transQTL/USED_input_ouput/QTLtools/Genotypes_metaData/LPS24/out.recode.vcf.gz --bed Delta_profile.bed.gz --cov covariance/covariance_methylation_10.txt --out deltaQTL_PC9_imputed.txt --permute 1000 --window 100000 --seed 123456' > Script_delta_imputed.sh

#------------------- chunck jobs
cd /t1-data/data/fairfaxlab/transcriptomics/RNAseq/eQTL/monocytes/analysis/QTL_analysis/Fastqtl/deltaQTL_QTLtools/Premutation_chunks_imputedGenotype/

for j in $(seq 1 20); do
     echo "QTLtools cis --vcf /t1-data/data/fairfaxlab/transcriptomics/RNAseq/eQTL/monocytes/analysis/QTL_analysis/Fastqtl/transQTL/USED_input_ouput/QTLtools/Genotypes_metaData/LPS24/out.recode.vcf.gz --bed /t1-data/data/fairfaxlab/transcriptomics/RNAseq/eQTL/monocytes/analysis/QTL_analysis/Fastqtl/deltaQTL_QTLtools/Delta_profile.bed.gz --cov /t1-data/data/fairfaxlab/transcriptomics/RNAseq/eQTL/monocytes/analysis/QTL_analysis/Fastqtl/deltaQTL_QTLtools/covariance/covariance_methylation_10.txt --permute 1000 --window 100000 --seed 123456 --chunk $j 20 --out /t1-data/data/fairfaxlab/transcriptomics/RNAseq/eQTL/monocytes/analysis/QTL_analysis/Fastqtl/deltaQTL_QTLtools/Premutation_chunks_imputedGenotype/nominals_"$j"_20.txt" > 'PreD_script_'$j'_20.sh'
     
    sed -i '1s/^/module load qtltools\ \n/' 'PreD_script_'$j'_20.sh'
    sed -i '1s/^/#$ -q batchq \n/' 'PreD_script_'$j'_20.sh'
    sed -i '1s/^/#$ -cwd \n/' 'PreD_script_'$j'_20.sh'
    sed -i '1 i\#!/bin/sh' 'PreD_script_'$j'_20.sh' 
 
done

for line in $(find -type f -name '*.sh'); do
sbatch $line
done

squeue -u nassisar
#-------------------

#find and merge
cd /t1-data/data/fairfaxlab/transcriptomics/RNAseq/eQTL/monocytes/analysis/QTL_analysis/Fastqtl/deltaQTL_QTLtools/Premutation_chunks_imputedGenotype/
find . -name '*.txt' -exec cat {} + >> 'Premutation_imputedGenotype_all.txt'
 


#============================ Run the conditional analysis imputed Genotype
echo '#!/bin/sh
#$ -cwd
#$ -q batchq
module load qtltools
cd /t1-data/data/fairfaxlab/transcriptomics/RNAseq/eQTL/monocytes/analysis/QTL_analysis/Fastqtl/deltaQTL_QTLtools/
module load qtltools
QTLtools cis --vcf /t1-data/data/fairfaxlab/transcriptomics/RNAseq/eQTL/monocytes/analysis/QTL_analysis/Fastqtl/transQTL/USED_input_ouput/QTLtools/Genotypes_metaData/LPS24/out.recode.vcf.gz --bed Delta_profile.bed.gz --cov covariance/covariance_methylation_10.txt --mapping PC_Selection/QTL_10.txt --out conditional_deltaQTL.txt' > Script_conditional.sh


#------------------- chunck jobs
cd /t1-data/data/fairfaxlab/transcriptomics/RNAseq/eQTL/monocytes/analysis/QTL_analysis/Fastqtl/deltaQTL_QTLtools/conditional_chunks_imputedGenotype/

for j in $(seq 1 20); do
     echo "QTLtools cis --vcf /t1-data/data/fairfaxlab/transcriptomics/RNAseq/eQTL/monocytes/analysis/QTL_analysis/Fastqtl/transQTL/USED_input_ouput/QTLtools/Genotypes_metaData/LPS24/out.recode.vcf.gz --bed /t1-data/data/fairfaxlab/transcriptomics/RNAseq/eQTL/monocytes/analysis/QTL_analysis/Fastqtl/deltaQTL_QTLtools/Delta_profile.bed.gz --cov /t1-data/data/fairfaxlab/transcriptomics/RNAseq/eQTL/monocytes/analysis/QTL_analysis/Fastqtl/deltaQTL_QTLtools/covariance/covariance_methylation_10.txt --mapping /t1-data/data/fairfaxlab/transcriptomics/RNAseq/eQTL/monocytes/analysis/QTL_analysis/Fastqtl/deltaQTL_QTLtools/PC_Selection/QTL_10.txt --window 100000 --chunk $j 20 --out /t1-data/data/fairfaxlab/transcriptomics/RNAseq/eQTL/monocytes/analysis/QTL_analysis/Fastqtl/deltaQTL_QTLtools/conditional_chunks_imputedGenotype/conditional_"$j"_20.txt" > 'ConD_script_'$j'_20.sh'
     
    sed -i '1s/^/module load qtltools\ \n/' 'ConD_script_'$j'_20.sh'
    sed -i '1s/^/#$ -q batchq \n/' 'ConD_script_'$j'_20.sh'
    sed -i '1s/^/#$ -cwd \n/' 'ConD_script_'$j'_20.sh'
    sed -i '1 i\#!/bin/sh' 'ConD_script_'$j'_20.sh' 
 
done

for line in $(find -type f -name '*.sh'); do
sbatch $line
done

squeue -u nassisar
#-------------------




#============================ Run the nominal analysis imputed Genotype
#------------------- chunck jobs
cd /t1-data/data/fairfaxlab/transcriptomics/RNAseq/eQTL/monocytes/analysis/QTL_analysis/Fastqtl/deltaQTL_QTLtools/nominal_chunks_imputedGenotype/

for j in $(seq 1 100); do
     echo "QTLtools cis --vcf /t1-data/data/fairfaxlab/transcriptomics/RNAseq/eQTL/monocytes/analysis/QTL_analysis/Fastqtl/Imputed_genotype/sub_set_all_SNPs_MAF_001_used/Imputed_Monocyte_genotype_LPS24.vcf.gz --bed /t1-data/data/fairfaxlab/transcriptomics/RNAseq/eQTL/monocytes/analysis/QTL_analysis/Fastqtl/deltaQTL_QTLtools/Delta_profile.bed.gz --cov /t1-data/data/fairfaxlab/transcriptomics/RNAseq/eQTL/monocytes/analysis/QTL_analysis/Fastqtl/deltaQTL_QTLtools/covariance/covariance_methylation_10.txt --nominal 0.001 --window 100000 --chunk $j 100 --out /t1-data/data/fairfaxlab/transcriptomics/RNAseq/eQTL/monocytes/analysis/QTL_analysis/Fastqtl/deltaQTL_QTLtools/nominal_chunks_imputedGenotype/mQTLnominal_"$j"_100.txt" > 'NomD_script_'$j'_100.sh'
     
    sed -i '1s/^/module load qtltools\ \n/' 'NomD_script_'$j'_100.sh'
    sed -i '1s/^/#$ -q batchq \n/' 'NomD_script_'$j'_100.sh'
    sed -i '1s/^/#$ -cwd \n/' 'NomD_script_'$j'_100.sh'
    sed -i '1 i\#!/bin/sh' 'NomD_script_'$j'_100.sh' 
 
done


for line in `ls -1 *.sh`; do
sbatch $line
done

squeue -u nassisar
#-------------------

#find and merge
cd /t1-data/data/fairfaxlab/transcriptomics/RNAseq/eQTL/monocytes/analysis/QTL_analysis/Fastqtl/deltaQTL_QTLtools/nominal_chunks_imputedGenotype/
find . -name 'mQTLnominal_*.txt' -exec cat {} + >> 'nominal_imputedGenotype_all.txt'
 

