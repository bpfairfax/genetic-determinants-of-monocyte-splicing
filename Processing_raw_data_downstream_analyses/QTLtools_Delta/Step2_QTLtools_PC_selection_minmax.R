
library(data.table)
files <- list.files('/t1-data/data/fairfaxlab/transcriptomics/RNAseq/eQTL/monocytes/analysis/QTL_analysis/Fastqtl/deltaQTL_QTLtools/PC_Selection/', pattern = '*.txt', full.names = TRUE)

setwd('/t1-data/data/fairfaxlab/transcriptomics/RNAseq/eQTL/monocytes/analysis/QTL_analysis/Fastqtl/deltaQTL_QTLtools/PC_Selection/')
for (j in 1:length(files)){
  print(j) 
  
  tem = fread(files[j], stringsAsFactors = F, header = F,fill=TRUE)
  name <- gsub('.*_|.txt','',files[j])
  tem = tem[!is.na(tem$V16),]
  tem = tem[which(tem$V16<0.001),]
  
  neQTLs = data.frame(PC=names(data)[j], neQTLs = dim(tem)[1])
  print(neQTLs)
  if(j==1){RESULT = neQTLs}else{RESULT = rbind(RESULT, neQTLs)}
  rm(neQTLs)
}

RESULT$PC = as.numeric(RESULT$PC)
RESULT = RESULT[order(RESULT$PC, decreasing = F),]

#----- local maxima (PC selection) ---------

#DATA
set.seed(42)

optimal_PC_num <- data.frame()

inflect <- function(x, threshold = 1){
  up <- sapply(1:threshold, function(n) c(x[-(seq(n))], rep(NA, n)))
  down <-sapply(-1:-threshold, function(n) c(rep(NA,abs(n)), x[-seq(length(x), length(x) - abs(n) + 1)]))
  a<- cbind(x,up,down)
  list(minima = which(apply(a, 1, min) == a[,1]), maxima = which(apply(a, 1, max) == a[,1]))
}

x = RESULT[,1]
y = RESULT[,2]

randomwalk=y

png(filename=paste('/t1-data/data/fairfaxlab/transcriptomics/RNAseq/eQTL/monocytes/analysis/QTL_analysis/Fastqtl/deltaQTL_QTLtools/PC_Selection/minmax.png',sep=''))

# Pick a desired threshold # to plot up to
n <- 2
# Generate Data
bottoms <- lapply(1:n, function(x) inflect(randomwalk, threshold = x)$minima)
tops <- lapply(1:n, function(x) inflect(randomwalk, threshold = x)$maxima)
# Color functions
cf.1 <- grDevices::colorRampPalette(c("pink","red"))
cf.2 <- grDevices::colorRampPalette(c("cyan","blue"))
plot(randomwalk, type = 'l', main = '')
for(i in 1:n){
  points(bottoms[[i]], randomwalk[bottoms[[i]]], pch = 16, col = cf.1(n)[i], cex = i/1.5)
}
for(i in 1:n){
  points(tops[[i]], randomwalk[tops[[i]]], pch = 16, col = cf.2(n)[i], cex = i/1.5)
}
legend("bottomright", legend = c("Minima",1:n,"Maxima",1:n), 
       pch = rep(c(NA, rep(16,n)), 2), col = c(1, cf.1(n),1, cf.2(n)), 
       pt.cex =c(rep(c(1, c(1:n) / 1.5), 2)), cex = .75, ncol = 2)
dev.off()

optimal_PC_num <- as.numeric(as.numeric(tops[[2]][1]))
write.table(optimal_PC_num, paste0('/t1-data/data/fairfaxlab/transcriptomics/RNAseq/eQTL/monocytes/analysis/QTL_analysis/Fastqtl/deltaQTL_QTLtools/PC_Selection//optimal_PC_'), row.names= FALSE, quote=FALSE, sep = "\t")



