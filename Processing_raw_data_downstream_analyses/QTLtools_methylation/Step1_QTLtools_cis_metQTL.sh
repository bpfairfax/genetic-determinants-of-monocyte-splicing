 
#--------------------------------- cis deltaQTL - QTLtools
#-- PC selection --

#--- create matrix of covariance
cd /t1-data/data/fairfaxlab/transcriptomics/RNAseq/eQTL/monocytes/analysis/QTL_analysis/Fastqtl/methylationQTL_QTLtools/
mkdir covariance_UT

for i in {2..51}  # i=2 => 1 PC
do

head -$i '/t1-data/data/fairfaxlab/transcriptomics/RNAseq/eQTL/monocytes/analysis/QTL_analysis/Fastqtl/methylationQTL_QTLtools/covariance_methylation_UT.txt' > 'covariance_UT/covariance_methylation_'$i'.txt'
done


#--- creat index files
module add samtools 
bgzip -i /t1-data/data/fairfaxlab/transcriptomics/RNAseq/eQTL/monocytes/analysis/QTL_analysis/Fastqtl/methylationQTL_QTLtools/Methylation_profile_UT.bed && tabix -p bed /t1-data/data/fairfaxlab/transcriptomics/RNAseq/eQTL/monocytes/analysis/QTL_analysis/Fastqtl/methylationQTL_QTLtools/Methylation_profile_UT.bed.gz

#--- QTLtools [PC selection - window 1MB]

cd /t1-data/data/fairfaxlab/transcriptomics/RNAseq/eQTL/monocytes/analysis/QTL_analysis/Fastqtl/methylationQTL_QTLtools/
mkdir script_UT/
mkdir PC_Selection_UT/

for i in {2..51}
do
echo '#!/bin/sh
#$ -cwd
#$ -q batchq
cd /t1-data/data/fairfaxlab/transcriptomics/RNAseq/eQTL/monocytes/analysis/QTL_analysis/Fastqtl/methylationQTL_QTLtools/
module load qtltools
QTLtools cis --vcf /t1-data/data/fairfaxlab/transcriptomics/RNAseq/eQTL/monocytes/analysis/QTL_analysis/Fastqtl/transQTL/USED_input_ouput/QTLtools/Genotypes_metaData/LPS24_arraySNPs/out.recode.vcf.gz --bed Methylation_profile_UT.bed.gz --cov covariance_UT/covariance_methylation_'$i'.txt --permute 1000 --out PC_Selection_UT/QTL_'$i'.txt --window 1000000 --seed 123456' > script_UT/Script_$i.sh
done

#------------------- submit jobs
 
cd /t1-data/data/fairfaxlab/transcriptomics/RNAseq/eQTL/monocytes/analysis/QTL_analysis/Fastqtl/methylationQTL_QTLtools/script_UT/

for line in $(find -type f -name '*.sh'); do
sbatch $line
done

squeue -u nassisar

#-------------------




#================================= imputed Genotype - premutation
 
UT: 12
LPS: 11

#------------------- UT
cd /t1-data/data/fairfaxlab/transcriptomics/RNAseq/eQTL/monocytes/analysis/QTL_analysis/Fastqtl/methylationQTL_QTLtools/

# Run the conditional analysis
echo '#!/bin/sh
#$ -cwd
#$ -q batchq
module load qtltools
cd /t1-data/data/fairfaxlab/transcriptomics/RNAseq/eQTL/monocytes/analysis/QTL_analysis/Fastqtl/methylationQTL_QTLtools/
QTLtools cis --vcf /t1-data/data/fairfaxlab/transcriptomics/RNAseq/eQTL/monocytes/analysis/QTL_analysis/Fastqtl/transQTL/USED_input_ouput/QTLtools/Genotypes_metaData/LPS24/out.recode.vcf.gz --bed Methylation_profile_UT.bed.gz --cov covariance_UT/covariance_methylation_12.txt --out mQTL_PC11_imputed_UT.txt --permute 1000 --window 100000 --seed 123456' > mQTL_PC11_imputed_UT.sh

#------------------- imputed Genotype - premutation - chunck jobs
cd /t1-data/data/fairfaxlab/transcriptomics/RNAseq/eQTL/monocytes/analysis/QTL_analysis/Fastqtl/methylationQTL_QTLtools/Premutation_chunks_imputedGenotype_UT/

for j in $(seq 1 20); do
     echo "QTLtools cis --vcf /t1-data/data/fairfaxlab/transcriptomics/RNAseq/eQTL/monocytes/analysis/QTL_analysis/Fastqtl/transQTL/USED_input_ouput/QTLtools/Genotypes_metaData/LPS24/out.recode.vcf.gz --bed /t1-data/data/fairfaxlab/transcriptomics/RNAseq/eQTL/monocytes/analysis/QTL_analysis/Fastqtl/methylationQTL_QTLtools/Methylation_profile_UT.bed.gz --cov /t1-data/data/fairfaxlab/transcriptomics/RNAseq/eQTL/monocytes/analysis/QTL_analysis/Fastqtl/methylationQTL_QTLtools/covariance_UT/covariance_methylation_12.txt --permute 1000 --window 100000 --seed 123456 --chunk $j 20 --out /t1-data/data/fairfaxlab/transcriptomics/RNAseq/eQTL/monocytes/analysis/QTL_analysis/Fastqtl/methylationQTL_QTLtools/Premutation_chunks_imputedGenotype_UT/nominals_"$j"_20.txt" > 'PreD_script_'$j'_20.sh'
     
    sed -i '1s/^/module load qtltools\ \n/' 'PreD_script_'$j'_20.sh'
    sed -i '1s/^/#$ -q batchq \n/' 'PreD_script_'$j'_20.sh'
    sed -i '1s/^/#$ -cwd \n/' 'PreD_script_'$j'_20.sh'
    sed -i '1 i\#!/bin/sh' 'PreD_script_'$j'_20.sh' 
 
done

for line in $(find -type f -name '*.sh'); do
sbatch $line
done

squeue -u nassisar
#-------------------
#find and merge
cd /t1-data/data/fairfaxlab/transcriptomics/RNAseq/eQTL/monocytes/analysis/QTL_analysis/Fastqtl/methylationQTL_QTLtools/Premutation_chunks_imputedGenotype_UT/
find . -name '*.txt' -exec cat {} + >> 'Premutated_imputedGenotype_all_UT.txt'


#------------------- LPS
cd /t1-data/data/fairfaxlab/transcriptomics/RNAseq/eQTL/monocytes/analysis/QTL_analysis/Fastqtl/methylationQTL_QTLtools/

# Run the conditional analysis
echo '#!/bin/sh
#$ -cwd
#$ -q batchq
module load qtltools
cd /t1-data/data/fairfaxlab/transcriptomics/RNAseq/eQTL/monocytes/analysis/QTL_analysis/Fastqtl/methylationQTL_QTLtools/
QTLtools cis --vcf /t1-data/data/fairfaxlab/transcriptomics/RNAseq/eQTL/monocytes/analysis/QTL_analysis/Fastqtl/transQTL/USED_input_ouput/QTLtools/Genotypes_metaData/LPS24/out.recode.vcf.gz --bed Methylation_profile_LPS.bed.gz --cov covariance_LPS/covariance_methylation_11.txt --out mQTL_PC10_imputed_LPS.txt --permute 1000 --window 100000 --seed 123456' > mQTL_PC10_imputed_LPS.sh

#------------------- imputed Genotype - premutation - chunck jobs
cd /t1-data/data/fairfaxlab/transcriptomics/RNAseq/eQTL/monocytes/analysis/QTL_analysis/Fastqtl/methylationQTL_QTLtools/Premutation_chunks_imputedGenotype_LPS/

for j in $(seq 1 20); do
     echo "QTLtools cis --vcf /t1-data/data/fairfaxlab/transcriptomics/RNAseq/eQTL/monocytes/analysis/QTL_analysis/Fastqtl/transQTL/USED_input_ouput/QTLtools/Genotypes_metaData/LPS24/out.recode.vcf.gz --bed /t1-data/data/fairfaxlab/transcriptomics/RNAseq/eQTL/monocytes/analysis/QTL_analysis/Fastqtl/methylationQTL_QTLtools/Methylation_profile_LPS.bed.gz --cov /t1-data/data/fairfaxlab/transcriptomics/RNAseq/eQTL/monocytes/analysis/QTL_analysis/Fastqtl/methylationQTL_QTLtools/covariance_LPS/covariance_methylation_11.txt --permute 1000 --window 100000 --seed 123456 --chunk $j 20 --out /t1-data/data/fairfaxlab/transcriptomics/RNAseq/eQTL/monocytes/analysis/QTL_analysis/Fastqtl/methylationQTL_QTLtools/Premutation_chunks_imputedGenotype_LPS/nominals_"$j"_20.txt" > 'PreD_script_'$j'_20.sh'
     
    sed -i '1s/^/module load qtltools\ \n/' 'PreD_script_'$j'_20.sh'
    sed -i '1s/^/#$ -q batchq \n/' 'PreD_script_'$j'_20.sh'
    sed -i '1s/^/#$ -cwd \n/' 'PreD_script_'$j'_20.sh'
    sed -i '1 i\#!/bin/sh' 'PreD_script_'$j'_20.sh' 
 
done

for line in $(find -type f -name '*.sh'); do
sbatch $line
done

squeue -u nassisar
#-------------------
#find and merge
cd /t1-data/data/fairfaxlab/transcriptomics/RNAseq/eQTL/monocytes/analysis/QTL_analysis/Fastqtl/methylationQTL_QTLtools/Premutation_chunks_imputedGenotype_LPS/
find . -name '*.txt' -exec cat {} + >> 'Premutated_imputedGenotype_all_LPS.txt'


#------------------- chunck jobs - LPS
cd /t1-data/data/fairfaxlab/transcriptomics/RNAseq/eQTL/monocytes/analysis/QTL_analysis/Fastqtl/methylationQTL_QTLtools/conditional_chunks_imputedGenotype_LPS/

for j in $(seq 1 20); do
     echo "QTLtools cis --vcf /t1-data/data/fairfaxlab/transcriptomics/RNAseq/eQTL/monocytes/analysis/QTL_analysis/Fastqtl/transQTL/USED_input_ouput/QTLtools/Genotypes_metaData/LPS24/out.recode.vcf.gz --bed /t1-data/data/fairfaxlab/transcriptomics/RNAseq/eQTL/monocytes/analysis/QTL_analysis/Fastqtl/methylationQTL_QTLtools/Methylation_profile_LPS.bed.gz --cov /t1-data/data/fairfaxlab/transcriptomics/RNAseq/eQTL/monocytes/analysis/QTL_analysis/Fastqtl/methylationQTL_QTLtools/covariance_LPS/covariance_methylation_11.txt --mapping /t1-data/data/fairfaxlab/transcriptomics/RNAseq/eQTL/monocytes/analysis/QTL_analysis/Fastqtl/methylationQTL_QTLtools/PC_Selection_LPS/QTL_11.txt --window 100000 --chunk $j 20 --out /t1-data/data/fairfaxlab/transcriptomics/RNAseq/eQTL/monocytes/analysis/QTL_analysis/Fastqtl/methylationQTL_QTLtools/conditional_chunks_imputedGenotype_LPS/conditional_"$j"_20.txt" > 'ConD_script_'$j'_20.sh'
     
    sed -i '1s/^/module load qtltools\ \n/' 'ConD_script_'$j'_20.sh'
    sed -i '1s/^/#$ -q batchq \n/' 'ConD_script_'$j'_20.sh'
    sed -i '1s/^/#$ -cwd \n/' 'ConD_script_'$j'_20.sh'
    sed -i '1 i\#!/bin/sh' 'ConD_script_'$j'_20.sh' 
 
done

for line in $(find -type f -name '*.sh'); do
sbatch $line
done

squeue -u nassisar
#-------------------

#find and merge
cd /t1-data/data/fairfaxlab/transcriptomics/RNAseq/eQTL/monocytes/analysis/QTL_analysis/Fastqtl/methylationQTL_QTLtools/nominal_chunks_imputedGenotype_LPS/
find . -name '*.txt' -exec cat {} + >> 'nominal_imputedGenotype_all_LPS.txt'





#============================ Run the nominal analysis imputed Genotype
#------------------- chunck jobs - LPS
cd /t1-data/data/fairfaxlab/transcriptomics/RNAseq/eQTL/monocytes/analysis/QTL_analysis/Fastqtl/methylationQTL_QTLtools/nominal_chunks_imputedGenotype_LPS/

for j in $(seq 1 100); do
     echo "QTLtools cis --vcf /t1-data/data/fairfaxlab/transcriptomics/RNAseq/eQTL/monocytes/analysis/QTL_analysis/Fastqtl/Imputed_genotype/sub_set_all_SNPs_MAF_001_used/Imputed_Monocyte_genotype_LPS24.vcf.gz --bed /t1-data/data/fairfaxlab/transcriptomics/RNAseq/eQTL/monocytes/analysis/QTL_analysis/Fastqtl/methylationQTL_QTLtools/Methylation_profile_LPS.bed.gz --cov /t1-data/data/fairfaxlab/transcriptomics/RNAseq/eQTL/monocytes/analysis/QTL_analysis/Fastqtl/methylationQTL_QTLtools/covariance_LPS/covariance_methylation_11.txt --nominal 0.001 --window 100000 --chunk $j 100 --out /t1-data/data/fairfaxlab/transcriptomics/RNAseq/eQTL/monocytes/analysis/QTL_analysis/Fastqtl/methylationQTL_QTLtools/nominal_chunks_imputedGenotype_LPS/mQTLnominal_"$j"_100.txt" > 'NomD_script_'$j'_100.sh'
     
    sed -i '1s/^/module load qtltools\ \n/' 'NomD_script_'$j'_100.sh'
    sed -i '1s/^/#$ -q batchq \n/' 'NomD_script_'$j'_100.sh'
    sed -i '1s/^/#$ -cwd \n/' 'NomD_script_'$j'_100.sh'
    sed -i '1 i\#!/bin/sh' 'NomD_script_'$j'_100.sh' 
 
done

for line in `ls -1 *.sh`; do
sbatch $line
done

squeue -u nassisar
#-------------------

#------------------- chunck jobs - UT
cd /t1-data/data/fairfaxlab/transcriptomics/RNAseq/eQTL/monocytes/analysis/QTL_analysis/Fastqtl/methylationQTL_QTLtools/nominal_chunks_imputedGenotype_UT/

for j in $(seq 1 100); do
     echo "QTLtools cis --vcf /t1-data/data/fairfaxlab/transcriptomics/RNAseq/eQTL/monocytes/analysis/QTL_analysis/Fastqtl/Imputed_genotype/sub_set_all_SNPs_MAF_001_used/Imputed_Monocyte_genotype_LPS24.vcf.gz --bed /t1-data/data/fairfaxlab/transcriptomics/RNAseq/eQTL/monocytes/analysis/QTL_analysis/Fastqtl/methylationQTL_QTLtools/Methylation_profile_UT.bed.gz --cov /t1-data/data/fairfaxlab/transcriptomics/RNAseq/eQTL/monocytes/analysis/QTL_analysis/Fastqtl/methylationQTL_QTLtools/covariance_UT/covariance_methylation_12.txt --nominal 0.001 --window 100000 --chunk $j 100 --out /t1-data/data/fairfaxlab/transcriptomics/RNAseq/eQTL/monocytes/analysis/QTL_analysis/Fastqtl/methylationQTL_QTLtools/nominal_chunks_imputedGenotype_UT/mQTLnominal_"$j"_100.txt" > 'NomD_script_'$j'_100.sh'
     
    sed -i '1s/^/module load qtltools\ \n/' 'NomD_script_'$j'_100.sh'
    sed -i '1s/^/#$ -q batchq \n/' 'NomD_script_'$j'_100.sh'
    sed -i '1s/^/#$ -cwd \n/' 'NomD_script_'$j'_100.sh'
    sed -i '1 i\#!/bin/sh' 'NomD_script_'$j'_100.sh' 
 
done

for line in `ls -1 *.sh`; do
sbatch $line
done

squeue -u nassisar
#-------------------

#find and merge
cd /t1-data/data/fairfaxlab/transcriptomics/RNAseq/eQTL/monocytes/analysis/QTL_analysis/Fastqtl/methylationQTL_QTLtools/nominal_chunks_imputedGenotype_UT/
find . -name '*.txt' -exec cat {} + >> 'nominal_imputedGenotype_all_UT.txt'



#================================= imputed Genotype - conditional [does not work]

#------------------- chunck jobs - UT
cd /t1-data/data/fairfaxlab/transcriptomics/RNAseq/eQTL/monocytes/analysis/QTL_analysis/Fastqtl/methylationQTL_QTLtools/conditional_chunks_imputedGenotype_LPS/

for j in $(seq 1 20); do
     echo "QTLtools cis --vcf /t1-data/data/fairfaxlab/transcriptomics/RNAseq/eQTL/monocytes/analysis/QTL_analysis/Fastqtl/transQTL/USED_input_ouput/QTLtools/Genotypes_metaData/LPS24/out.recode.vcf.gz --bed /t1-data/data/fairfaxlab/transcriptomics/RNAseq/eQTL/monocytes/analysis/QTL_analysis/Fastqtl/methylationQTL_QTLtools/Methylation_profile_UT.bed.gz --cov /t1-data/data/fairfaxlab/transcriptomics/RNAseq/eQTL/monocytes/analysis/QTL_analysis/Fastqtl/methylationQTL_QTLtools/covariance_UT/covariance_methylation_12.txt --mapping /t1-data/data/fairfaxlab/transcriptomics/RNAseq/eQTL/monocytes/analysis/QTL_analysis/Fastqtl/methylationQTL_QTLtools/PC_Selection_UT/QTL_12.txt --window 100000 --chunk $j 20 --out /t1-data/data/fairfaxlab/transcriptomics/RNAseq/eQTL/monocytes/analysis/QTL_analysis/Fastqtl/methylationQTL_QTLtools/conditional_chunks_imputedGenotype_UT/conditional_"$j"_20.txt" > 'ConD_script_'$j'_20.sh'
     
    sed -i '1s/^/module load qtltools\ \n/' 'ConD_script_'$j'_20.sh'
    sed -i '1s/^/#$ -q batchq \n/' 'ConD_script_'$j'_20.sh'
    sed -i '1s/^/#$ -cwd \n/' 'ConD_script_'$j'_20.sh'
    sed -i '1 i\#!/bin/sh' 'ConD_script_'$j'_20.sh' 
 
done

for line in $(find -type f -name '*.sh'); do
sbatch $line
done

squeue -u nassisar
#-------------------
