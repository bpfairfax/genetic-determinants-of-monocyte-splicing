
#----------------------
library(data.table)
TP_ALL = fread(paste0('/t1-data/data/fairfaxlab/transcriptomics/RNAseq/eQTL/monocytes/analysis/QTL_analysis/Fastqtl/methylationQTL_QTLtools/nominal_chunks_imputedGenotype_LPS/nominal_imputedGenotype_all_LPS_annotated.txt'), stringsAsFactors = F)
TP_ALL$SNP_ID = gsub('.*;', '', TP_ALL$SNP_ID) 
TP_ALL = TP_ALL[TP_ALL$FDR < 0.01,]

Num_genes = unique(TP_ALL$CpG)
length(Num_genes)

i=1
TP_ALL_sub = TP_ALL[which(TP_ALL$CpG == Num_genes[i]),]

library(ieugwasr) #R/3.5.0-newgcc
clump_input = cbind.data.frame(rsid = TP_ALL_sub$SNP_ID, chr_name = TP_ALL_sub$SNP_chr, chrom_start = TP_ALL_sub$SNP_start, pval = TP_ALL_sub$FDR)
r <- sapply(clump_input, is.factor)
clump_input[r] <- lapply(clump_input[r], as.character)
str(clump_input)
clumped_input <- ld_clump(clump_input)

profile = TP_ALL_sub[which(TP_ALL_sub$SNP_ID %in% clumped_input$rsid),]

write.table(profile, paste0('/t1-data/data/fairfaxlab/transcriptomics/RNAseq/eQTL/monocytes/analysis/QTL_analysis/Fastqtl/methylationQTL_QTLtools/nominal_chunks_imputedGenotype_LPS/ld_clump/ld_clump_',i,'.txt'), quote = F, row.names = F, sep = '\t')


