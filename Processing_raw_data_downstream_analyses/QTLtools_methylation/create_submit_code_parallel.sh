
DIR=/t1-data/data/fairfaxlab/transcriptomics/RNAseq/eQTL/monocytes/analysis/QTL_analysis/Fastqtl/methylationQTL_QTLtools/nominal_chunks_imputedGenotype_LPS/ld_clump/

mkdir $DIR'/script/'
cd $DIR'/script/'

for j in {1..179610}
do
cp -fr Step4_ld_clump_LPS.R 'Step4_ld_clump_LPS_'$j'.R' -f
# Replace line 11
sed -i '11s/i=1/i='$j'/' 'Step4_ld_clump_LPS_'$j'.R'
done


#LPS
#[1] 179610

#UT
#[1] 179223

rm -f Step4_ld_clump_LPS.R

# Creat .sh script
for f in $(find -type f -name '*.R'); do
NAME=$(echo $f | awk -F/ '{print $NF}')

echo -e '#!/bin/sh
#$ -cwd
#$ -q batchq
module add R-cbrg
cd '$DIR'/script/
R --vanilla < '${f}' > '${f}'.txt' > ${f}'.sh'
sbatch $NAME'.sh'
done

 