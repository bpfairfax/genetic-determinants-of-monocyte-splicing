
######################## gene transcripts of an indicated gene
library(biomaRt)
ensembl <- useMart("ensembl",dataset="hsapiens_gene_ensembl")   #for CBRG: ensembl <- useMart("ENSEMBL_MART_ENSEMBL",dataset="hsapiens_gene_ensembl", host = "jul2015.archive.ensembl.org")
filters = listFilters(ensembl)
GTF <- getBM(attributes=c("hgnc_symbol", "ensembl_gene_id", "ensembl_transcript_id",'chromosome_name','start_position', 'end_position', 'band', "gene_biotype"), mart = ensembl)
colnames(GTF) = c('gene_name', 'gene_id', 'transcript_id', 'seqnames', 'start', 'end', 'strand', "gene_biotype")
GTF = GTF[which(GTF$seqnames %in% c(1:22,'X','Y','MT')),]
dim(GTF)
TP='EP300'
GENE='LYZ'
transcripts = unique(GTF[which(GTF$gene_name == TP),'transcript_id'])
unique(GTF[which(GTF$gene_name == GENE),'gene_id'])

######################## get alleles of an indeicated SNP
library(haploR)
querySNP = queryHaploreg(query='rs10784774', ldThresh = 1, ldPop = "EUR")

######################## case study ########################

for (j in 1:length(transcripts)) {

REF=querySNP$ref
ALT=querySNP$alt

Gene2=unique(GTF[which(GTF$gene_name == 'LYZ'),'gene_id'])
Gene1=transcripts[j]
SNP=querySNP$rsID

print(Gene1)

tryCatch({
  
# treat=c('LPS24', 'IFN', 'UT')
treat=c('UT')
for(i in 1:length(treat))
{
  treatment=treat[i]
  #-------------------
  
  library(data.table)
  expression = fread(paste0('/t1-data/data/fairfaxlab/transcriptomics/RNAseq/eQTL/monocytes/analysis/QTL_analysis/Fastqtl/RESULTS_USED/Expression/Input_files_gene/expression_',treatment,'.txt'), stringsAsFactors = F)
  expression = as.data.frame(expression)
  row.names(expression) = expression$id
  expression = expression[,-1]
  dim(expression)
  
  library(data.table)
  expression_tQTL = fread(paste0('/t1-data/data/fairfaxlab/transcriptomics/RNAseq/eQTL/monocytes/analysis/QTL_analysis/Fastqtl/RESULTS_USED/Expression/Input_files_transcript/expression_',treatment,'.txt'), stringsAsFactors = F)
  expression_tQTL = as.data.frame(expression_tQTL)
  row.names(expression_tQTL) = expression_tQTL$id
  expression_tQTL = expression_tQTL[,-1]
  dim(expression_tQTL)
 
  expression = expression[!duplicated(row.names(expression)),]
  dim(expression)
  expression_tQTL = expression_tQTL[!duplicated(row.names(expression_tQTL)),]
  dim(expression_tQTL)
  
  TP_GENOTYPE = fread(paste0('/t1-data/data/fairfaxlab/transcriptomics/RNAseq/eQTL/monocytes/analysis/QTL_analysis/Fastqtl/RESULTS_USED/Genotype//Monocyte_imputed_matrixQTL_Allsamples_justSNPs_format2_USED_for_BOXPLOT.txt'),skip = paste0(SNP, '_'), nrows = 1, stringsAsFactors = F, header = F)
  TP_GENOTYPE$V1 = gsub('_', '', TP_GENOTYPE$V1)
  
  GENOTYPE_sample_names = fread(paste0('/t1-data/data/fairfaxlab/transcriptomics/RNAseq/eQTL/monocytes/analysis/QTL_analysis/Fastqtl/RESULTS_USED/Genotype//Monocyte_imputed_matrixQTL_Allsamples_justSNPs_format2.txt'),nrows = 1, stringsAsFactors = F)
  colnames(TP_GENOTYPE) = colnames(GENOTYPE_sample_names)
  
  expression=as.data.frame(expression)
  TP_GENOTYPE=as.data.frame(TP_GENOTYPE)
  expression = expression[,which(colnames(expression) %in% colnames(TP_GENOTYPE))]
  TP_GENOTYPE = TP_GENOTYPE[,which(colnames(TP_GENOTYPE) %in% colnames(expression))]
  
  expression_tQTL = expression_tQTL[,which(colnames(expression_tQTL) %in% colnames(TP_GENOTYPE))]
  
  dim(TP_GENOTYPE)
  dim(expression) 
  dim(expression_tQTL)
  
  library(stringr)
  
  Gene1_id=Gene1
  Gene2_id=Gene2
  
  TP_EXPESSION_Gene1 = expression_tQTL[which(row.names(expression_tQTL) %in% Gene1),]
  dim(TP_EXPESSION_Gene1) 
  
  TP_EXPESSION_Gene2 = expression[which(row.names(expression) %in% Gene2),]
  dim(TP_EXPESSION_Gene2) 
  
  TP_GENOTYPE_sub = TP_GENOTYPE#[which(row.names(TP_GENOTYPE) == SNP),]
  dim(TP_GENOTYPE_sub)
  
  TP_EXPESSION_Gene1 = as.data.frame(TP_EXPESSION_Gene1)
  TP_EXPESSION_Gene2 = as.data.frame(TP_EXPESSION_Gene2)
  TP_GENOTYPE_sub = as.data.frame(TP_GENOTYPE_sub)
  TP_GENOTYPE_sub
  
  #=============== visualization type 1
  #---------- Ref homozygote
  
  expr0=data.frame(Gene1=as.numeric(TP_EXPESSION_Gene1[,which(TP_GENOTYPE_sub[1,]==0)]), 
                   Gene2=as.numeric(TP_EXPESSION_Gene2[,which(TP_GENOTYPE_sub[1,]==0)]) )
  expr1=data.frame(Gene1=as.numeric(TP_EXPESSION_Gene1[,which(TP_GENOTYPE_sub[1,]==1)]), 
                   Gene2=as.numeric(TP_EXPESSION_Gene2[,which(TP_GENOTYPE_sub[1,]==1)]) )
  expr2=data.frame(Gene1=as.numeric(TP_EXPESSION_Gene1[,which(TP_GENOTYPE_sub[1,]==2)]), 
                   Gene2=as.numeric(TP_EXPESSION_Gene2[,which(TP_GENOTYPE_sub[1,]==2)]) )
  
  expr0$Genotype = rep(paste0('0: ' ,REF,'_', REF), dim(expr0)[1])
  expr1$Genotype = rep(paste0('1: ' ,REF,'_', ALT), dim(expr1)[1])
  expr2$Genotype = rep(paste0('2: ' ,ALT,'_', ALT), dim(expr2)[1])
  
  input.plot = do.call(rbind,list(expr0,expr1,expr2))
  
  input.plot$Genotype = factor(input.plot$Genotype, levels = unique(input.plot$Genotype))
  input.plot$State = rep(treatment, dim(input.plot)[1])
  
  assign(treatment, input.plot)
  
}

# input.plot = rbindlist(list(IFN, LPS24, UT), use.names=TRUE, fill=TRUE)
# input.plot$State = factor(input.plot$State,levels = c('UT', 'LPS24', 'IFN'))
input.plot = UT
input.plot$State = factor(input.plot$State,levels = c('UT'))

# input.plot$Gene2 = log10(input.plot$Gene2)
# input.plot$Gene1 = log10(input.plot$Gene1)

setwd('/t1-data/data/fairfaxlab/transcriptomics/RNAseq/eQTL/monocytes/analysis/QTL_analysis/co-expression_QTL/BoxPlots/')
library(ggpubr)
library(ggplot2)
# Scatterplot
theme_set(theme_bw())# pre-set the bw theme.
g <- ggplot(input.plot, aes( Gene1,Gene2)) +
  labs(subtitle="Allele-specific correlation for expresseion values per genotype",
       title="", x = Gene1, y = Gene2)

p = g + geom_jitter(aes(col=Genotype, size=Gene1)) +
  geom_smooth(aes(col=Genotype), method="lm", se=F) + scale_color_manual(values=c("#999999", "#E69F00", "#56B4E9")) + geom_rug(aes(color =Genotype))+
  theme(axis.text=element_text(size=20), axis.title=element_text(size=20), plot.subtitle = element_text(size = 20 ),legend.title=element_text(size=20), legend.text=element_text(size=18))#+ facet_wrap(~Genotype) , face="bold"

library(grDevices)
library(grid)
if(length(treat) == 1)
{
  pdf(file=paste0(GENE,'_', TP,'_', Gene1,'_',treatment,"_RNAseq_GeneTP_correlationplot_2.pdf"), width = 10, height = 10, useDingbats = F)
  print(p + facet_wrap(~ State))
  dev.off()  
}else{
  pdf(file=paste0(GENE,'_', TP,'_', Gene1,'_',treatment,"_RNAseq_GeneTP_correlationplot_2.pdf"), width = 20, height = 10, useDingbats = F)
  print(p + facet_wrap(~ State))
  dev.off()
}


}, error=function(e){})
}
