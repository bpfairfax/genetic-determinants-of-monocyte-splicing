
module load fastqtl
cd /folder/

while read -r -a line; do
echo $line

#--- eQTL analysis without covariant

fastQTL --vcf 'genotype_'$line'_sorted.vcf.gz' --bed '/expression_'$line'.bed.gz' --commands 100 'commands_'$line'_0.sh' --out '/QTL_'$line'_0' --threshold 0.01 --window 1e6 --permute 1000 --seed 123456789

sed -i '1s/^/module load fastqtl\ \n/' 'commands_'$line'_0.sh'
sed -i '1s/^/#$ -m eas \n/' 'commands_'$line'_0.sh'
sed -i '1s/^/#$ -M nassisar \n/' 'commands_'$line'_0.sh'
sed -i '1s/^/#$ -q batchq \n/' 'commands_'$line'_0.sh'
sed -i '#$ -cwd' 'commands_'$line'_0.sh'
sed -i '#!/bin/sh' 'commands_'$line'_0.sh'
sed -i 'ulimit -c 0' 'commands_'$line'_0.sh'

qsub 'commands_'$line'_0.sh'

#--- eQTL analysis with covariant

for i in {2..51}  # i=2 => 1 PCdo
 
head -$i 'covariance_'$line'.txt' > 'covariance_'$line'_'$i'.txt'

fastQTL --vcf '/Monocyte_genotype_'$line'_sorted.vcf.gz' --bed '/expression_'$line'.bed.gz' --commands 100 'commands_'$line'_'$i'.sh' --out '/QTL_'$line'_'$i --threshold 0.01 --window 1e6 --permute 1000 --seed 123456789 --cov '/covariance_'$line'_'$i'.txt'

sed -i '1s/^/module load fastqtl\ \n/' 'commands_'$line'_'$i'.sh'
sed -i '1s/^/#$ -m eas \n/' 'commands_'$line'_'$i'.sh'
sed -i '1s/^/#$ -M nassisar \n/' 'commands_'$line'_'$i'.sh'
sed -i '1s/^/#$ -q batchq \n/' 'commands_'$line'_'$i'.sh'
sed -i '#$ -cwd' 'commands_'$line'_'$i'.sh'
sed -i '#!/bin/sh' 'commands_'$line'_'$i'.sh'
sed -i 'ulimit -c 0' 'commands_'$line'_'$i'.sh'

qsub 'commands_'$line'_'$i'.sh'
done
done < 'list_states.txt'  # LPS, IFN, and UT
