
#--------------- QTLtools
#ssh cbrglogin1
module load qtltools   
cd /folder/
QTLtools trans --vcf Genotype.vcf.gz --bed SDA_450.bed.gz --out SDA_results_COM450 --normal --nominal --threshold 0.05 --window 1000000
