#INSTALL
# install CrossMap to default location. In Linux/Unix, this location is like:
# /home/user/lib/python2.7/site-packages/
$ python setup.py install

# or you can install CrossMap to a specified location:
$ python setup.py install --root=/CrossMap-0.2.7/

#RUN
module add python/2.7.5
export PYTHONPATH=/lib/python2.7/site-packages:$PYTHONPATH.

export PATH=/lib/python2.7/site-packages:$PATH

cd /folder/
python CrossMap.py

#---- bed
#Usage:
#CrossMap.py bed input_chain_file input_bed_file [output_file]

python /CrossMap.py bed /hg19ToHg38.over.chain.gz /INPUT_liftover_1.bed /INPUT_liftover_1_gh19.bed

python /CrossMap.py bed /hg38ToHg19.over.chain.gz /INPUT_liftover_2.bed /INPUT_liftover_2_gh19.bed

#---- vcf
python /CrossMap.py vcf /hg19ToHg38.over.chain.gz /All.genos.610.631290.151130.vcf /GRCh38.fa /Monocyte_GRCh38.vcf
 
