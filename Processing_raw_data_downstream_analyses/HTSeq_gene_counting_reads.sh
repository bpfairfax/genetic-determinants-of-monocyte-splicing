
DIR=/folder/
OUTPUT=/folder/

cd $DIR
for NAME in $(find . -name "*_nodup_properPairs_NH.bam" -printf "%f\n" | sed 's/_nodup_properPairs_NH.bam//'); do
for ADDRESS in $(find . -name $NAME'_nodup_properPairs_NH.bam'); do 

echo "$NAME"
echo "$ADDRESS"

echo '#!/bin/sh
#$ -cwd
#$ -q batchq
#$ -M nassisar
#$ -m eas

module add HTSeq/0.6.1p1
module add python

cd /folder/
python -m HTSeq.scripts.count --format=bam --minaqual=0 --stranded=no --type=exon --mode=union' $ADDRESS '/folder/GRCh38.gtf > /folder/'$NAME'_gene_read_count.txt' > $OUTPUT'CD_'$NAME'.sh'

done 
done



