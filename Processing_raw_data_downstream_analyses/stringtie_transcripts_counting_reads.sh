 
cd /folder/ 

#create mergelist
find . -name '*_nodup_properPairs_NH.gtf' >> /folder/TP_readcount/mergelist.txt

#Create merged gtf file
module add stringtie
module add hisat
stringtie --merge -p 8 -G /GRCh38.gtf -o '/folder/TP_readcount/stringtie_merged.gtf' /folder/TP_readcount/mergelist.txt
#-----------

DIR=/folder/ 
cd $DIR
for NAME in $(find . -name "*_nodup_properPairs_NH.gtf" -printf "%f\n" | sed 's/_nodup_properPairs_NH.gtf//'); do
echo "$NAME"
for i in `ls $NAME'_nodup_properPairs_NH.bam'`; do
echo $DIR$i
 
#Run stringtie
mkdir '/folder/TP_readcount/'$NAME

echo '#!/bin/sh
#$ -cwd
#$ -q batchq
#$ -M nassisar
#$ -m eas
module add stringtie
module add python
cd /folder/TP_readcount/'$NAME'; 
stringtie '$DIR$i '-G /folder/TP_readcount/stringtie_merged.gtf -A gene_abund.txt -e -B -p 8 > /folder/TP_readcount/'$NAME'/'$NAME'.gtf' > /folder/TP_readcount/$NAME.sh

done
done


