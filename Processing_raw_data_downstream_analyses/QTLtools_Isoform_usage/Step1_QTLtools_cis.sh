 
#--------------------------------- cis deltaQTL - QTLtools - arraySNPs

#============================ PC selection

#--- create matrix of covariance
cd /t1-data/data/fairfaxlab/transcriptomics/RNAseq/eQTL/monocytes/analysis/QTL_analysis/Fastqtl/Isoform_usage_QTLtools/
mkdir covariance

treatment='MET1m'
for i in {2..51}  # i=2 => 1 PC
do

head -$i '/t1-data/data/fairfaxlab/transcriptomics/RNAseq/eQTL/monocytes/analysis/QTL_analysis/Fastqtl/Isoform_usage_QTLtools/IU_covariates_'$treatment'.txt' > '/t1-data/data/fairfaxlab/transcriptomics/RNAseq/eQTL/monocytes/analysis/QTL_analysis/Fastqtl/Isoform_usage_QTLtools/covariance/IU_covariates_'$treatment'_'$i'.txt'
done

#--- creat index files
module add samtools 
bgzip -i /t1-data/data/fairfaxlab/transcriptomics/RNAseq/eQTL/monocytes/analysis/QTL_analysis/Fastqtl/Isoform_usage_QTLtools/$treatment.bed && tabix -p bed /t1-data/data/fairfaxlab/transcriptomics/RNAseq/eQTL/monocytes/analysis/QTL_analysis/Fastqtl/Isoform_usage_QTLtools/$treatment.bed.gz



#--- QTLtools
treatment='UT'
DIR='/t1-data/data/fairfaxlab/transcriptomics/RNAseq/eQTL/monocytes/analysis/QTL_analysis/Fastqtl/Isoform_usage_QTLtools/'

mkdir $DIR$treatment'/PC_Selection/'
mkdir $DIR$treatment'/PC_Selection/script/'

cd $DIR

for i in {2..51}
do
echo '#!/bin/sh
#$ -cwd
#$ -q batchq
#SBATCH --ntasks=1
#SBATCH --mem=1G
module load qtltools
cd '$DIR'
QTLtools cis --vcf /t1-data/data/fairfaxlab/transcriptomics/RNAseq/eQTL/monocytes/analysis/QTL_analysis/Fastqtl/GENE_microarray/Monocyte_genotype_'$treatment'_sorted.vcf.gz --bed '$treatment'.bed.gz --cov covariance/IU_covariates_'$treatment'_'$i'.txt --permute 1000 --out /t1-data/data/fairfaxlab/transcriptomics/RNAseq/eQTL/monocytes/analysis/QTL_analysis/Fastqtl/Isoform_usage_QTLtools/'$treatment'/PC_Selection/uQTL_'$i'.txt --window 1000000 --seed 123456' > '/t1-data/data/fairfaxlab/transcriptomics/RNAseq/eQTL/monocytes/analysis/QTL_analysis/Fastqtl/Isoform_usage_QTLtools/'$treatment'/PC_Selection/script/Script_'$i'.sh'
done

#------------------- submit jobs
 
cd '/t1-data/data/fairfaxlab/transcriptomics/RNAseq/eQTL/monocytes/analysis/QTL_analysis/Fastqtl/Isoform_usage_QTLtools/'$treatment'/PC_Selection/script/'

for line in $(find -type f -name '*.sh'); do
sbatch $line
done

squeue -u nassisar

#-------------------



#============================ imputed Genotype - permutation
treatment='UT'
PC=8
#IFN 7, LPS 6, MET 11, UT 8
DIR='/t1-data/data/fairfaxlab/transcriptomics/RNAseq/eQTL/monocytes/analysis/QTL_analysis/Fastqtl/Isoform_usage_QTLtools/'

mkdir $DIR$treatment'/permutation_pass/'
mkdir $DIR$treatment'/permutation_pass/script/'

cd $DIR$treatment'/permutation_pass/script/'
#------------------- chunck jobs

for j in $(seq 1 20); do
     echo 'QTLtools cis --vcf /t1-data/data/fairfaxlab/transcriptomics/RNAseq/eQTL/monocytes/analysis/QTL_analysis/Fastqtl/GENE_microarray/Monocyte_genotype_'$treatment'_sorted.vcf.gz --bed /t1-data/data/fairfaxlab/transcriptomics/RNAseq/eQTL/monocytes/analysis/QTL_analysis/Fastqtl/Isoform_usage_QTLtools/'$treatment'.bed.gz --cov /t1-data/data/fairfaxlab/transcriptomics/RNAseq/eQTL/monocytes/analysis/QTL_analysis/Fastqtl/Isoform_usage_QTLtools/covariance/IU_covariates_'$treatment'_'$PC'.txt --permute 1000 --window 100000 --seed 123456 --chunk '$j' 20 --out /t1-data/data/fairfaxlab/transcriptomics/RNAseq/eQTL/monocytes/analysis/QTL_analysis/Fastqtl/Isoform_usage_QTLtools/'$treatment'/permutation_pass/uQTL_permutation_pass_'$j'.txt' > 'uQTL_permutation_pass_script_'$j'.sh'
     
    sed -i '1s/^/module load qtltools\ \n/' 'uQTL_permutation_pass_script_'$j'.sh'
    sed -i '1s/^/#$ -q batchq \n/' 'uQTL_permutation_pass_script_'$j'.sh'
    sed -i '1s/^/#$ -cwd \n/' 'uQTL_permutation_pass_script_'$j'.sh'
    sed -i '1 i\#!/bin/sh' 'uQTL_permutation_pass_script_'$j'.sh'
 
done

cd $DIR$treatment'/permutation_pass/script/'
for line in $(find -type f -name '*.sh'); do
sbatch $line
done

squeue -u nassisar
#-------------------


#============================ imputed Genotype - nominal
DIR='/t1-data/data/fairfaxlab/transcriptomics/RNAseq/eQTL/monocytes/analysis/QTL_analysis/Fastqtl/Isoform_usage_QTLtools/'

mkdir $DIR$treatment'/nominal_pass/'
mkdir $DIR$treatment'/nominal_pass/script/'

cd $DIR$treatment'/nominal_pass/script/'
#------------------- chunck jobs

for j in $(seq 1 20); do
     echo 'QTLtools cis --vcf /t1-data/data/fairfaxlab/transcriptomics/RNAseq/eQTL/monocytes/analysis/QTL_analysis/Fastqtl/Imputed_genotype/sub_set_all_SNPs_MAF_001_used/Imputed_Monocyte_genotype_'$treatment'.vcf.gz --bed /t1-data/data/fairfaxlab/transcriptomics/RNAseq/eQTL/monocytes/analysis/QTL_analysis/Fastqtl/Isoform_usage_QTLtools/'$treatment'.bed.gz --cov /t1-data/data/fairfaxlab/transcriptomics/RNAseq/eQTL/monocytes/analysis/QTL_analysis/Fastqtl/Isoform_usage_QTLtools/covariance/IU_covariates_'$treatment'_'$PC'.txt --nominal 0.001 --window 100000 --chunk '$j' 20 --out /t1-data/data/fairfaxlab/transcriptomics/RNAseq/eQTL/monocytes/analysis/QTL_analysis/Fastqtl/Isoform_usage_QTLtools/'$treatment'/nominal_pass/uQTL_nominal_pass_'$j'.txt' > 'uQTL_nominal_pass_script_'$j'.sh'
     
    sed -i '1s/^/module load qtltools\ \n/' 'uQTL_nominal_pass_script_'$j'.sh'
    sed -i '1s/^/#$ -q batchq \n/' 'uQTL_nominal_pass_script_'$j'.sh'
    sed -i '1s/^/#$ -cwd \n/' 'uQTL_nominal_pass_script_'$j'.sh'
    sed -i '1 i\#!/bin/sh' 'uQTL_nominal_pass_script_'$j'.sh'
 
done

cd $DIR$treatment'/nominal_pass/script/'
for line in $(find -type f -name '*.sh'); do
sbatch $line
done

squeue -u nassisar
#-------------------

#------------------- find and merge
treatment='LPS24'
cd '/t1-data/data/fairfaxlab/transcriptomics/RNAseq/eQTL/monocytes/analysis/QTL_analysis/Fastqtl/Isoform_usage_QTLtools/'$treatment'/nominal_pass/'
find . -name '*.txt' -exec cat {} + >> /t1-data/data/fairfaxlab/transcriptomics/RNAseq/eQTL/monocytes/analysis/QTL_analysis/Fastqtl/Isoform_usage_QTLtools/Nominal_imputedGenotype_$treatment.txt

cd '/t1-data/data/fairfaxlab/transcriptomics/RNAseq/eQTL/monocytes/analysis/QTL_analysis/Fastqtl/Isoform_usage_QTLtools/'$treatment'/permutation_pass/'
find . -name '*.txt' -exec cat {} + >> /t1-data/data/fairfaxlab/transcriptomics/RNAseq/eQTL/monocytes/analysis/QTL_analysis/Fastqtl/Isoform_usage_QTLtools/Permutation_imputedGenotype_$treatment.txt


#------------------- test
# Run the conditional analysis
echo '#!/bin/sh
#$ -cwd
#$ -q batchq
module load qtltools
cd '$DIR'
QTLtools cis --vcf /t1-data/data/fairfaxlab/transcriptomics/RNAseq/eQTL/monocytes/analysis/QTL_analysis/Fastqtl/GENE_microarray/Monocyte_genotype_'$treatment'_sorted.vcf.gz --bed '$treatment'.bed.gz --cov covariance/IU_covariates_'$treatment'_'$PC'.txt --permute 1000 --out /t1-data/data/fairfaxlab/transcriptomics/RNAseq/eQTL/monocytes/analysis/QTL_analysis/Fastqtl/Isoform_usage_QTLtools/'$treatment'/permutation_pass/uQTL_permutation_pass.txt --window 100000 --seed 123456' > '/t1-data/data/fairfaxlab/transcriptomics/RNAseq/eQTL/monocytes/analysis/QTL_analysis/Fastqtl/Isoform_usage_QTLtools/'$treatment'/permutation_pass/script/Script_permutation_pass.sh'
