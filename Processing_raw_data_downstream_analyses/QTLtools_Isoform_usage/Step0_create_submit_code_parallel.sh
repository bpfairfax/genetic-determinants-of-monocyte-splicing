
DIR=/t1-data/data/fairfaxlab/transcriptomics/RNAseq/eQTL/monocytes/analysis/QTL_analysis/Fastqtl/Isoform_usage_QTLtools/UT/IU/

mkdir $DIR
mkdir $DIR'/script/'
cd $DIR'/script/'

met 94
ifn 130
lps 167

for j in {1..167}
do
cp -fr Script.R 'Script'$j'.R' -f
# Replace line 11
sed -i '34s/i=1/i='$j'/' 'Script'$j'.R'
done

rm -f Script.R

# Creat .sh script
for f in $(find -type f -name '*.R'); do
NAME=$(echo $f | awk -F/ '{print $NF}')  # remove ./

echo -e '#!/bin/sh
#$ -cwd
#$ -q batchq
#SBATCH --ntasks=1
#SBATCH --mem=5G
module add R-cbrg
cd '$DIR'/script/
R --vanilla < '${f}' > '${f}'.txt' > ${f}'.sh'
sbatch $NAME'.sh'
done

 