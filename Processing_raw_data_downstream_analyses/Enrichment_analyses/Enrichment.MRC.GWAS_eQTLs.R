#------------------------ backend data
library(TwoSampleMR)
library(MRInstruments)
library(haploR)

# List available GWASs
ao <- available_outcomes()
ao = ao[!is.na(ao$ncase),]
dim(ao)
setwd("/Users/isar.nassiri/Desktop")
write.table(ao, 'List_of_19473_GWASsummaryStat.txt', quote = F, row.names = F, sep = '\t')

UKBIO = ao[grep('UKB', ao$id),]
UKBIO=UKBIO[grep('Cancer|cancer|immun|Immun|inflammat|Inflammat', UKBIO$trait),]
#UKBIO=UKBIO[-grep('Diagnoses', UKBIO$trait),]
UKBIO <- UKBIO[grepl("European", UKBIO$population), ]
dim(UKBIO)

table(ao$subcategory)
toMatch <- c("Autoimmune / inflammatory",
 'Cofactors and vitamins',
 "Haemotological", "Cancer",
 "Immune system", "Immune cell subset frequency", "Metabolites ratio", "Nucleotide","Paediatric disease")
allDBs <- ao[grepl(paste(toMatch,collapse="|"), ao$subcategory),]
allDBs <- allDBs[grepl("European", allDBs$population), ]
dim(allDBs)

UKBIO = rbind(allDBs, UKBIO)
UKBIO = UKBIO[-grep('Illnesses of|self-reported|Self-reported|Non-cancer|non-cancer|noninflammatory|Noninflammatory', UKBIO$trait),]
dim(UKBIO)
table(UKBIO$consortium)
UKBIO

#--------------------------- query
treatment = 'UT'
if(treatment == 'IFN'){query_nCases = 139} else if (treatment == 'LPS24') {query_nCases = 176 } else {query_nCases = 176 }

library(data.table)
profile = fread(paste0('/Volumes/Fairfaxlab1/Laptop/Article_figures/input_files/Window_100kb/eQTL_',treatment,'_annotated_clumped_leadSNPs.txt'), stringsAsFactors = F)
profile = profile[profile$FDR < 1e-3,]
colnames(profile)
# [1] "gene_id"           "SNP_ID"            "Distance_Gene_SNP"
# [4] "bpval"             "slop"              "FDR"              
# [7] "seqnames"          "start"             "end"              
# [10] "strand"            "gene_name"         "gene_biotype"     
# [13] "SNP_CHROM"         "SNP_POS"           "REF"              
# [16] "ALT"               "count_ALLELE_0"    "count_ALLELE_1"   
# [19] "count_ALLELE_2"    "mac"               "maf"              
# [22] "index"


#-- If you donot use the LD peak SNPs
library(ieugwasr) #R/3.5.0-newgcc
clump_input = cbind.data.frame(rsid = profile$SNP_ID, chr_name = profile$SNP_CHROM, chrom_start = profile$SNP_POS, pval = profile$FDR)
r <- sapply(clump_input, is.factor)
clump_input[r] <- lapply(clump_input[r], as.character)
str(clump_input)
clumped_input <- ld_clump(clump_input)

profile = profile[which(profile$SNP_ID %in% clumped_input$rsid),]

# INPUT = QUERY
INPUT = cbind.data.frame(SNP = profile$SNP_ID,
                         beta.exposure = profile$slop,
                         se.exposure = 0.01,
                         effect_allele.exposure = profile$REF,
                         other_allele.exposure = profile$ALT,
                         eaf.exposure = profile$maf,
                         pval.exposure = profile$FDR,
                         gene.exposure = rep('.', dim(profile)[1]))

INPUT = cbind.data.frame(INPUT, rep('eQTL', dim(INPUT)[1]), rep('eQTL', dim(INPUT)[1]))
colnames(INPUT) = c('SNP','beta.exposure','se.exposure','effect_allele.exposure','other_allele.exposure','eaf.exposure','pval.exposure','gene.exposure','exposure','id.exposure')

r <- sapply(INPUT, is.factor)
INPUT[r] <- lapply(INPUT[r], as.character)

str(INPUT)

dir.create(paste0('/Volumes/Fairfaxlab1/Laptop/Article_figures/FIGURES_TABLES-ciseQTLs/Trait_enrichment_results_ciseQTLs/ciseQTL_trait_enrichment/eQTL_trait_enrichment_',treatment,'/'))
setwd(paste0('/Volumes/Fairfaxlab1/Laptop/Article_figures/FIGURES_TABLES-ciseQTLs/Trait_enrichment_results_ciseQTLs/ciseQTL_trait_enrichment/eQTL_trait_enrichment_',treatment,'/'))

getwd()
i=1
ok=0
RESULTS = data.frame()

library(biomaRt)
snpdetail=useMart("ENSEMBL_MART_SNP", dataset="hsapiens_snp",host="grch37.ensembl.org", path="/biomart/martservice",archive=FALSE)

for(i in 1:length(UKBIO$id))
{
  input_enrichment = chd_out_dat = NULL
  
  tryCatch({
    
    chd_out_dat <- extract_outcome_data(
      snps = INPUT$SNP,
      outcomes = UKBIO$id[i]
    )
    
  }, error=function(e){})
  
  if(!is.null(chd_out_dat))
  {
    input_enrichment <- harmonise_data(
      exposure_dat = INPUT, 
      outcome_dat = chd_out_dat 
    )
    # colnames(INPUT)
    # chd_out_dat[,grep('proxy', colnames(chd_out_dat))]#[c(1:3,7:10,12,16)]
    
    input_enrichment = input_enrichment[which(input_enrichment$pval.outcome<1e-3),]
    
    #-------------- coloc
    if(dim(input_enrichment)[1]>0)
    {
      
      if(!is.null(input_enrichment$proxy.outcome))
      {
        if(length(which(input_enrichment$proxy.outcome))>0)
        {
          
          proxies = input_enrichment[which(input_enrichment$proxy.outcome),]
          proxies$SNP = as.character(proxies$SNP)
          
          #----- coloc per SNP
          library(coloc)
          for(j in 1:(dim(proxies)[1]))
          {
            
            if(is.na(proxies$eaf.outcome[j]))
            {
              SNPs_rsID_MAF = getBM(attributes=c('chr_name', 'chrom_start',"minor_allele_freq","refsnp_id"),filters="snp_filter",value=as.character(proxies$SNP[j]), snpdetail)
              if(dim(SNPs_rsID_MAF)[1]>0){proxies$eaf.outcome[j] = SNPs_rsID_MAF$minor_allele_freq}
            }
            
            if(is.na(proxies$eaf.exposure[j]))
            {
              SNPs_rsID_MAF = getBM(attributes=c('chr_name', 'chrom_start',"minor_allele_freq","refsnp_id"),filters="snp_filter",value=as.character(proxies$SNP[j]), snpdetail)
              if(dim(SNPs_rsID_MAF)[1]>0){proxies$eaf.outcome[j] = SNPs_rsID_MAF$minor_allele_freq}
            }
            
            COLOC_Per_SNP <- coloc.abf(list(pvalues=as.numeric(proxies$pval.exposure[j]), N=query_nCases, MAF=as.numeric(proxies$eaf.exposure[j]), type="quant"),
                                       list(pvalues=as.numeric(proxies$pval.outcome[j]), N=proxies$ncase.outcome[j], MAF=as.numeric(proxies$eaf.outcome[j]), type="quant"))
            if(COLOC_Per_SNP$summary['PP.H4.abf']<0.5){print('1k'); input_enrichment = input_enrichment[-which(input_enrichment$proxy_snp.outcome == proxies$proxy_snp.outcome[j]),]}else{print('0k')}
          }
          
        }}}
    
    if(dim(input_enrichment)[1]>0)
    {
      
      #-------------- mendelian randomization test
      # remove duplicate summries by selecting most informative one
      # input_enrichment<-power.prune(input_enrichment, method.size=F)
      
      # enrichment
      res <- mr(input_enrichment)
      
      if(i==1 & ok == 0){RESULTS = res; ok=1}
      if(i!=1 & ok == 0){RESULTS = res; ok=1}
      if(i!=1 & ok == 1){RESULTS=rbind(res,RESULTS)}
      
      write.table(res, paste0(UKBIO$id[i], '_', treatment, '_Enrichment_traits_coloc_MRT.txt'), quote = F, row.names = F, sep = '\t')
      
    }}#if(!is.null(chd_out_dat))
  
}
write.table(RESULTS, paste0('eQTL_',treatment,'_Enrichment_traits_ALL.txt'), quote = F, row.names = F, sep = '\t')


