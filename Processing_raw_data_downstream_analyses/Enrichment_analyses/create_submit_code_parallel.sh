
DIR=/t1-data/data/fairfaxlab/transcriptomics/RNAseq/eQTL/monocytes/analysis/QTL_analysis/Fastqtl/Monocyte_Paper_related_files/mQTL/UT/

mkdir $DIR'/script/'
cd $DIR'/script/'
# LPS 1200 , UT 1328
for j in {1..1328}
do
cp -fr Enrichment_ChromatinState_GenomicFeature_enrichment_mQTL.R 'Enrichment_ChromatinState_GenomicFeature_enrichment_mQTL_'$j'.R' -f
sed -i '70s/i=1/i='$j'/' 'Enrichment_ChromatinState_GenomicFeature_enrichment_mQTL_'$j'.R'
done

rm -f Enrichment_ChromatinState_GenomicFeature_enrichment_mQTL.R

# Creat .sh script
for f in $(find -type f -name '*.R'); do
NAME=$(echo $f | awk -F/ '{print $NF}')

echo -e '#!/bin/sh
#$ -cwd
#$ -q batchq
module add R-cbrg
cd '$DIR'/script/
R --vanilla < '${f}' > '${f}'.txt' > ${f}'.sh'
sbatch $NAME'.sh'
done
 