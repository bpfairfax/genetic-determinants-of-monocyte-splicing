
# setwd(dirname(rstudioapi::getActiveDocumentContext()$path))
zScore <- function (cntA, totA, cntB, totB) {
  #calculate
  avgProportion <- (cntA + cntB) / (totA + totB)
  probA <- cntA/totA
  probB <- cntB/totB
  SE <- sqrt(avgProportion * (1-avgProportion)*(1/totA + 1/totB))
  zScore <- (probA-probB) / SE
  return (zScore)
}

treatment = 'IFN'
dir.create(paste0('/t1-data/data/fairfaxlab/transcriptomics/RNAseq/eQTL/monocytes/analysis/QTL_analysis/Fastqtl/Monocyte_Paper_related_files/chromatin_genomeregions_enrich_tQTL/',treatment,'/'))
setwd(paste0('/t1-data/data/fairfaxlab/transcriptomics/RNAseq/eQTL/monocytes/analysis/QTL_analysis/Fastqtl/Monocyte_Paper_related_files/chromatin_genomeregions_enrich_tQTL/',treatment,'/'))

#--------------------------- read inputs

library(data.table)
library(GenomicRanges)
# https://egg2.wustl.edu/roadmap/data/byFileType/chromhmmSegmentations/ChmmModels/coreMarks/jointModel/final/download/
states_bed = fread("/t1-data/data/fairfaxlab/transcriptomics/RNAseq/eQTL/monocytes/analysis/DataBases/Genotyping1000/E029_15_coreMarks_hg38lift_dense.bed")  # hg19

chromatin_states = GRanges(sample = states_bed[[4]],
                           seqnames = gsub('chr','',states_bed[[1]]),
                           ranges = IRanges(states_bed[[2]], states_bed[[3]]),
                           state = states_bed[[4]])
length(chromatin_states)
head(chromatin_states)

genomic_regions = fread('/t1-data/data/fairfaxlab/transcriptomics/RNAseq/eQTL/monocytes/analysis/DataBases/genomic_regions_DB_TxDb_Biomart_K562_hg38.txt', stringsAsFactors = F, header = T)
genomic_regions$seqnames = gsub('chr','',genomic_regions$seqnames)
genomic_regions = genomic_regions[which(genomic_regions$seqnames %in% c(1:22, 'X')),]
genomic_regions = GRanges(sample = genomic_regions$sample,
                          seqnames = genomic_regions$seqnames,
                          ranges = IRanges(genomic_regions$start, genomic_regions$end),
                          state = genomic_regions$state)

SNPs_Imputed = fread('/t1-data/data/fairfaxlab/transcriptomics/RNAseq/eQTL/monocytes/analysis/QTL_analysis/Fastqtl/RESULTS_USED/Genotype/MAF_imputed_Allsamples_TYPED.txt')
SNPs_Imputed_subject <- GRanges(Rle(as.character(SNPs_Imputed$CHROM)), IRanges(SNPs_Imputed$POS, width=2), rsID = SNPs_Imputed$ID)

eQTL = fread(paste0('/t1-data/data/fairfaxlab/transcriptomics/RNAseq/eQTL/monocytes/analysis/QTL_analysis/Fastqtl/RESULTS_USED/tQTL/Window_100kb/tQTL_',treatment,'_All.txt'))
eQTL$index = paste(eQTL$SNP_ID, eQTL$gene_name, sep = '_')
dim(eQTL)

#--- exclude eQTLs
profile_eQTL = fread(paste0('/t1-data/data/fairfaxlab/transcriptomics/RNAseq/eQTL/monocytes/analysis/QTL_analysis/Fastqtl/RESULTS_USED/eQTL/eQTL_',treatment,'_All.txt'), stringsAsFactors = F)
profile_eQTL$index = paste(profile_eQTL$SNP_ID, profile_eQTL$gene_name, sep = '_')

dim(eQTL)
eQTL = eQTL[-which(eQTL$index %in% profile_eQTL$index),]
dim(eQTL)
eQTL = eQTL[eQTL$FDR < 0.001,]
eQTL = eQTL[!is.na(eQTL$seqnames),]
genes = unique(eQTL$gene_id)
length(genes)
eQTL = as.data.frame(eQTL)
SNPs_Imputed = as.data.frame(SNPs_Imputed)

i=1
#--------------------------------- CS enrichment
# for (i in 1:length(genes)) {

print(genes[i])
print(i)
#--- select eQTLs
eQTL_sub = eQTL[which(eQTL$gene_id == genes[i]),]
dim(eQTL_sub)

#--- Foreground_rsIDs
Foreground_rsIDs <- GRanges(paste0(eQTL_sub$seqnames,':',eQTL_sub$SNP_POS,'-',eQTL_sub$SNP_POS), rsID = eQTL_sub$SNP_ID)
length(Foreground_rsIDs)

#--- Background_rsIDs - select SNPs any a 1Mb windows
posQuerySNP <- GRanges(paste0(eQTL_sub$seqnames[1],':',eQTL_sub$start[1],'-',eQTL_sub$start[1])) + 100000  # for foreground tQTL 100kb, background 1mb
fo <- findOverlaps(query=posQuerySNP, subject=SNPs_Imputed_subject, type="any")
fo
Background_rsIDs = SNPs_Imputed_subject[subjectHits(fo),]

#--- remove foreground from background
Background_rsIDs = Background_rsIDs[-which(Background_rsIDs$rsID %in% Foreground_rsIDs$rsID),]
length(Background_rsIDs)

#--- enrich foreground
fo <- findOverlaps(query=Foreground_rsIDs, subject=chromatin_states, type="any")
foreground_CS = chromatin_states[subjectHits(fo),]
#--- enrich background
fo <- findOverlaps(query=Background_rsIDs, subject=chromatin_states, type="any")
Background_CS = chromatin_states[subjectHits(fo),]

CSs = unique(foreground_CS$sample)

if(length(CSs)>0)
{
  
for(t in 1:length(CSs))
{
  cntA=length(which(foreground_CS$sample==CSs[t]))
  totA=length(Foreground_rsIDs)
  cntB=length(which(Background_CS$sample==CSs[t]))
  totB=length(Background_rsIDs)
  
  table = c(cntA, totA, cntB, totB)
  dim(table)<-c(2,2)
  fishert <- fisher.test(table)
  
  z_score = zScore(cntA, totA, cntB, totB)
  
  temp = data.frame(gene = genes[i], CS = CSs[t], p = fishert$p.value, oddsratio = fishert$estimate, CIl= fishert$conf.int[1], CIh = fishert$conf.int[2], zScore = z_score, FE = cntA, FA = totA, BE = cntB, BA = totB, treatment = treatment)
  
  if(t==1){RESULTs = temp}else{RESULTs = rbind(RESULTs, temp)}
  
  print(z_score)
}
# if(i==1){RESULTs2 = RESULTs}else{RESULTs2 = rbind(RESULTs2, RESULTs)}
write.table(RESULTs, paste0('CS_Enrichment_Result_',genes[i],'_',treatment,'.txt'), quote = F, row.names = F, sep = '\t')
  
}
#------------------- genomic features

#--- enrich foreground
fo <- findOverlaps(query=Foreground_rsIDs, subject=genomic_regions, type="any")
foreground_GR = genomic_regions[subjectHits(fo),]

#--- enrich background
fo <- findOverlaps(query=Background_rsIDs, subject=genomic_regions, type="any")
Background_GR = genomic_regions[subjectHits(fo),]

if(length(foreground_GR)>0)
{
  GRs = unique(foreground_GR$sample)
  for(t in 1:length(GRs))
  {
    cntA=length(which(foreground_GR$sample==GRs[t]))
    totA=length(foreground_GR)
    cntB=length(which(Background_GR$sample==GRs[t]))
    totB=length(Background_GR)
    
    table = c(cntA, totA, cntB, totB)
    dim(table)<-c(2,2)
    fishert <- fisher.test(table)
    
    z_score = zScore(cntA, totA, cntB, totB)
    
    temp = data.frame(gene = genes[i], GR = GRs[t], p = fishert$p.value, oddsratio = fishert$estimate, CIl= fishert$conf.int[1], CIh = fishert$conf.int[2], zScore = z_score, FE = cntA, FA = totA, BE = cntB, BA = totB, treatment = treatment)
    
    if(t==1){RESULTsGR = temp}else{RESULTsGR = rbind(RESULTsGR, temp)}
    
    print(z_score)
  }
  # if(i==1){RESULTsGR2 = RESULTsGR}else{RESULTsGR2 = rbind(RESULTsGR2, RESULTsGR)}
  write.table(RESULTsGR, paste0('GR_Enrichment_Result_',genes[i],'_',treatment,'.txt'), quote = F, row.names = F, sep = '\t')
}# is.null

# }
