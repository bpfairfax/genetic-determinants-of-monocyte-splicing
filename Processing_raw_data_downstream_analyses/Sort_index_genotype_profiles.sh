cd /folder/
 
for NAME in $(find . -name "Imputed_Monocyte_genotype_*" -printf "%f\n" | sed 's/.recode.vcf//'); do
for ADDRESS in $(find . -name $NAME'.recode.vcf'); do  

echo $NAME
echo $ADDRESS

echo -e '#!/bin/sh
#$ -cwd
#$ -q batchq
#$ -M nassisar
#$ -m eas

module add python/2.7.5
module add samtools 
module add vcftools 

cd /folder/

#--- sort vcf
vcf-sort' $NAME'.recode.vcf >' $NAME'.recode_sorted.vcf 

#--- creat index files
bgzip' $NAME'.recode_sorted.vcf && tabix -p vcf' $NAME'.recode_sorted.vcf.gz' > /folder/'create_index_'$NAME'.sh'

done
done

