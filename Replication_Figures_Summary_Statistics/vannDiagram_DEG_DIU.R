library(data.table)

#------------ iso
IFN = fread('/Volumes/Isar_Nassiri_Fairfax_lab/RESULTS_USED/Monocyte_Differential_expression_analysis/Isoform_switching/Merged_results/Isoform_switching_IFN.txt', stringsAsFactors = F, header = T)
IFN = IFN[IFN$p_value < 0.01,]
length(unique(IFN$gene_id))
table(IFN$gene_id == 'ENSG00000185499')

LPS24 = fread('/Volumes/Isar_Nassiri_Fairfax_lab/RESULTS_USED/Monocyte_Differential_expression_analysis/Isoform_switching/Merged_results/Isoform_switching_LPS.txt', stringsAsFactors = F, header = T)
LPS24 = LPS24[LPS24$p_value < 0.01,]
table(LPS24$gene_id == 'ENSG00000185499')
length(unique(LPS24$gene_id))

All_iso = unique(c(IFN$gene_id, LPS24$gene_id))
ALLIso = rbind(IFN, LPS24)
length(unique(ALLIso$isoform_id))
length(unique(ALLIso$gene_id))

#------------ DEG
IFN_DEGs = fread('/Volumes/Isar_Nassiri_Fairfax_lab/RESULTS_USED/Monocyte_Differential_expression_analysis/DEG/IFN/DEG_CT_IFN.txt', stringsAsFactors = F, header = T)
IFN_DEGs = IFN_DEGs[IFN_DEGs$padj < 0.01,]
IFN_DEGs = IFN_DEGs[IFN_DEGs$padj != 0,]
IFN_DEGs = IFN_DEGs[IFN_DEGs$gene_biotype == 'protein_coding',]
length(unique(IFN_DEGs$gene_id))

LPS24_DEGs = fread('/Volumes/Isar_Nassiri_Fairfax_lab/RESULTS_USED/Monocyte_Differential_expression_analysis/DEG/LPS24/DEG_CT_LPS24.txt', stringsAsFactors = F, header = T)
LPS24_DEGs = LPS24_DEGs[LPS24_DEGs$padj < 0.01,]
LPS24_DEGs = LPS24_DEGs[LPS24_DEGs$padj != 0,]
LPS24_DEGs = LPS24_DEGs[LPS24_DEGs$gene_biotype == 'protein_coding',]
length(unique(LPS24_DEGs$gene_id))

All_DEG = unique(c(IFN_DEGs$gene_id, LPS24_DEGs$gene_id))
length(setdiff(unique(ALLIso$gene_id), All_DEG))

#------------ all genes
all_genes = fread('/Volumes/Isar_Nassiri_Fairfax_lab/RESULTS_USED/Monocyte_Differential_expression_analysis/DEG/gene_raw_readcount_UT.txt', stringsAsFactors = F, header = T)
length(unique(all_genes$id))

library(biomaRt)
ensembl <- useMart("ensembl",dataset="hsapiens_gene_ensembl")   #for CBRG: ensembl <- useMart("ENSEMBL_MART_ENSEMBL",dataset="hsapiens_gene_ensembl", host = "jul2015.archive.ensembl.org")
filters = listFilters(ensembl)
GTF <- getBM(attributes=c("hgnc_symbol", "ensembl_gene_id", "gene_biotype"), mart = ensembl)
colnames(GTF) = c('gene_name', 'gene_id', "gene_biotype") 
GTF = GTF[GTF$gene_biotype == 'protein_coding',]
dim(GTF)

all_genes = all_genes[which(all_genes$id %in% unique(c(GTF$gene_id,All_DEG,All_iso))),]
dim(all_genes)
all_genes = unique( all_genes$id)

length(All_DEG)
length(All_iso)
length(intersect(all_genes, All_iso))
length(intersect(all_genes, All_DEG))

#------------ venn diagram 
length(All_DEG)
length(All_iso)

#upload library
library(VennDiagram)
setwd('/Users/isar.nassiri/Desktop/')
venn.plot <- venn.diagram(
  list( DEGs =All_DEG, DIU = All_iso),
  main = paste0(''),
  filename = NULL,
  output = F, 
  lwd = 1.5, 
  lty = "dashed",
  cex = 2, cat.cex = 2,
  fill = c( '#66CC99','#FF9966'),
  print.mode = 'percent'
);
 
grid.draw(venn.plot)
getwd()
library(grDevices)
pdf(file=paste0('proportions_intersect_allGenes_DEG_DUI.pdf'))
grid.draw(venn.plot)
dev.off()

 








