
DIR=/t1-data/data/fairfaxlab/transcriptomics/RNAseq/eQTL/monocytes/analysis/QTL_analysis/Fastqtl/RESULTS_USED/input_files_replication_ciseQTL/proxy_SNP_eQTL_cisMonocyte/

mkdir $DIR'/script/'
cd $DIR'/script/'

for j in {1..59727}
do
cp -fr FIND_PROXY_SNP.R 'FIND_PROXY_SNP_'$j'.R' -f
sed -i '22s/i=1/i='$j'/' 'FIND_PROXY_SNP_'$j'.R'
done


#> length(Num_genes)
#[1] 47214

rm -f FIND_PROXY_SNP.R

# Creat .sh script
for f in $(find -type f -name '*.R'); do
NAME=$(echo $f | awk -F/ '{print $NF}')

echo -e '#!/bin/sh
#$ -cwd
#$ -q batchq
module add R-cbrg
cd '$DIR'/script/
R --vanilla < '${f}' > '${f}'.txt' > ${f}'.sh'
sbatch $NAME'.sh'
done


 


 