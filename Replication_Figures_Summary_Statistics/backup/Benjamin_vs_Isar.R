
#============================== read eQTLs
library(data.table)
setwd('/t1-data/data/fairfaxlab/transcriptomics/RNAseq/eQTL/monocytes/analysis/QTL_analysis/Fastqtl/RESULTS_USED/eQTL/')
Gene_ALL_LPS24_sub = fread('eQTL_LPS24_annotated_clumped_leadSNPs.txt', stringsAsFactors = F)
Gene_ALL_LPS24_sub = Gene_ALL_LPS24_sub[Gene_ALL_LPS24_sub$FDR<0.01,]

Gene_ALL_IFN_sub = fread('eQTL_IFN_annotated_clumped_leadSNPs.txt', stringsAsFactors = F)
Gene_ALL_IFN_sub = Gene_ALL_IFN_sub[Gene_ALL_IFN_sub$FDR<0.01,]

Gene_ALL_UT_sub = fread('eQTL_UT_annotated_clumped_leadSNPs.txt', stringsAsFactors = F)
Gene_ALL_UT_sub = Gene_ALL_UT_sub[Gene_ALL_UT_sub$FDR<0.01,]

eQTLs <- list(A = paste(Gene_ALL_IFN_sub$gene_id, Gene_ALL_IFN_sub$SNP_ID, sep = '_'),
              B = paste(Gene_ALL_UT_sub$gene_id, Gene_ALL_UT_sub$SNP_ID, sep = '_'),
              C = paste(Gene_ALL_LPS24_sub$gene_id, Gene_ALL_LPS24_sub$SNP_ID, sep = '_'))
alleQTLs =  do.call(c, eQTLs)
alleQTLs = unique(alleQTLs)
length(alleQTLs) #total of x eQTL

#============================== read tQTL
library(data.table)
setwd('/t1-data/data/fairfaxlab/transcriptomics/RNAseq/eQTL/monocytes/analysis/QTL_analysis/Fastqtl/RESULTS_USED/tQTL/Window_100kb/')
TP_ALL_LPS24_sub = fread('Peak_SNPs_LD_tQTL_LPS24.txt', stringsAsFactors = F)
TP_ALL_LPS24_sub = TP_ALL_LPS24_sub[grep('ENST',TP_ALL_LPS24_sub$gene_id),]
TP_ALL_LPS24_sub = TP_ALL_LPS24_sub[TP_ALL_LPS24_sub$FDR<0.01,]

TP_ALL_IFN_sub = fread('Peak_SNPs_LD_tQTL_IFN.txt', stringsAsFactors = F)
TP_ALL_IFN_sub = TP_ALL_IFN_sub[grep('ENST',TP_ALL_IFN_sub$gene_id),]
TP_ALL_IFN_sub = TP_ALL_IFN_sub[TP_ALL_IFN_sub$FDR<0.01,]

TP_ALL_UT_sub = fread('Peak_SNPs_LD_tQTL_UT.txt', stringsAsFactors = F)
TP_ALL_UT_sub = TP_ALL_UT_sub[grep('ENST',TP_ALL_UT_sub$gene_id),]
TP_ALL_UT_sub = TP_ALL_UT_sub[TP_ALL_UT_sub$FDR<0.01,]

tQTLs <- list(A = paste(TP_ALL_IFN_sub$gene_id, TP_ALL_IFN_sub$SNP_ID, sep = '_'),
              B = paste(TP_ALL_UT_sub$gene_id, TP_ALL_UT_sub$SNP_ID, sep = '_'),
              C = paste(TP_ALL_LPS24_sub$gene_id, TP_ALL_LPS24_sub$SNP_ID, sep = '_'))
alltQTLs =  do.call(c, tQTLs)
alltQTLs = unique(alltQTLs)



#============================== We found 2578 genes (29.8% of all genes tested) have transcript QTL specific to the activated state. 

############## BENJMAIN
ifnU<-TP_ALL_IFN_sub$gene_name[!TP_ALL_IFN_sub$gene_name%in%TP_ALL_UT_sub$gene_name]
ifnU<-ifnU[!ifnU%in%TP_ALL_LPS24_sub$gene_name]
length(unique(ifnU))
ifnU<-unique(ifnU)

UtU<-TP_ALL_UT_sub$gene_name[!TP_ALL_UT_sub$gene_name%in%TP_ALL_IFN_sub$gene_name]
UtU<-UtU[!UtU%in%TP_ALL_LPS24_sub$gene_name]
length(unique(UtU))
UtU<-unique(UtU)

lpsU<-TP_ALL_LPS24_sub$gene_name[!TP_ALL_LPS24_sub$gene_name%in%TP_ALL_IFN_sub$gene_name]
lpsU<-lpsU[!lpsU%in%TP_ALL_UT_sub$gene_name]
length(unique(lpsU))
lpsU<-unique(lpsU)

lengthU<-sum(length(ifnU),length(UtU),length(lpsU))
lengthU

############## ISAR
genetQTLs <- list(A = TP_ALL_IFN_sub$gene_name, B = TP_ALL_UT_sub$gene_name, C = TP_ALL_LPS24_sub$gene_name)
# CS genes in tQTLs
genesInCStQTLs = lapply(1:length(genetQTLs), function(n) setdiff(genetQTLs[[n]], unlist(genetQTLs[-n])))
names(genesInCStQTLs) = c('IFN', 'UT', 'LPS24')
genesInCStQTLs =  do.call(c, genesInCStQTLs)
# all genes in tQTLs
AllGenestQTLs =  do.call(c, genetQTLs)

length(unique(genesInCStQTLs))/length(unique(AllGenestQTLs))
length(unique(genesInCStQTLs))


#------ per state - Condition Specific peak tQTLs

############## BENJMAIN
ifnU<-TP_ALL_IFN_sub$gene_id[!TP_ALL_IFN_sub$gene_id%in%TP_ALL_UT_sub$gene_id]
ifnU<-ifnU[!ifnU%in%TP_ALL_LPS24_sub$gene_id]
length(unique(ifnU))
ifnU<-unique(ifnU)

UtU<-TP_ALL_UT_sub$gene_id[!TP_ALL_UT_sub$gene_id%in%TP_ALL_IFN_sub$gene_id]
UtU<-UtU[!UtU%in%TP_ALL_LPS24_sub$gene_id]
length(unique(UtU))
UtU<-unique(UtU)

lpsU<-TP_ALL_LPS24_sub$gene_id[!TP_ALL_LPS24_sub$gene_id%in%TP_ALL_IFN_sub$gene_id]
lpsU<-lpsU[!lpsU%in%TP_ALL_UT_sub$gene_id]
length(unique(lpsU))
lpsU<-unique(lpsU)

############## ISAR
length(unique(setdifftQTLs[[1]]))
length(unique(setdifftQTLs[[2]]))
length(unique(setdifftQTLs[[3]]))

#------ some transcripts associate with more than one peak SNPs, which I report them as separate tQTLs, therefore I have slightly more CS tQTLs
TP_ALL_IFN_sub_CS = TP_ALL_IFN_sub[which(TP_ALL_IFN_sub$gene_id %in% setdifftQTLs[[1]]),]
dim(TP_ALL_IFN_sub_CS) 
TP_ALL_UT_sub_CS = TP_ALL_UT_sub[which(TP_ALL_UT_sub$gene_id %in% setdifftQTLs[[2]]),]
dim(TP_ALL_UT_sub_CS)
TP_ALL_LPS24_sub_CS = TP_ALL_LPS24_sub[which(TP_ALL_LPS24_sub$gene_id %in% setdifftQTLs[[3]]),]
dim(TP_ALL_LPS24_sub_CS)


#==============================  Condition Specific eQTLs
eQTLs <- list(A = Gene_ALL_IFN_sub$gene_id,
              B = Gene_ALL_UT_sub$gene_id,
              C = Gene_ALL_LPS24_sub$gene_id)
setdiffeQTLs = lapply(1:length(eQTLs), function(n) setdiff(eQTLs[[n]], unlist(eQTLs[-n])))
names(setdiffeQTLs) = c('IFN', 'UT', 'LPS24')

#------ per state
Gene_ALL_IFN_sub_CS = Gene_ALL_IFN_sub[which(Gene_ALL_IFN_sub$gene_id %in% setdiffeQTLs[[1]]),]
dim(Gene_ALL_IFN_sub_CS)
Gene_ALL_UT_sub_CS = Gene_ALL_UT_sub[which(Gene_ALL_UT_sub$gene_id %in% setdiffeQTLs[[2]]),]
dim(Gene_ALL_UT_sub_CS)
Gene_ALL_LPS24_sub_CS = Gene_ALL_LPS24_sub[which(Gene_ALL_LPS24_sub$gene_id %in% setdiffeQTLs[[3]]),]
dim(Gene_ALL_LPS24_sub_CS)

############## ISAR - Condition Specific eQTLs
length(unique(setdiffeQTLs[[1]]))
length(unique(setdiffeQTLs[[2]]))
length(unique(setdiffeQTLs[[3]]))

############## BENJMAIN
ifnU<-Gene_ALL_IFN_sub$gene_id[!Gene_ALL_IFN_sub$gene_id%in%Gene_ALL_UT_sub$gene_id]
ifnU<-ifnU[!ifnU%in%Gene_ALL_LPS24_sub$gene_id]
length(unique(ifnU))
ifnU<-unique(ifnU)

UtU<-Gene_ALL_UT_sub$gene_id[!Gene_ALL_UT_sub$gene_id%in%Gene_ALL_IFN_sub$gene_id]
UtU<-UtU[!UtU%in%Gene_ALL_LPS24_sub$gene_id]
length(unique(UtU))
UtU<-unique(UtU)

lpsU<-Gene_ALL_LPS24_sub$gene_id[!Gene_ALL_LPS24_sub$gene_id%in%Gene_ALL_IFN_sub$gene_id]
lpsU<-lpsU[!lpsU%in%Gene_ALL_UT_sub$gene_id]
length(unique(lpsU))
lpsU<-unique(lpsU)




