# Genetic determinants of monocyte splicing are enriched for disease susceptibility loci including for COVID-19 #

Scripts used in the analysis and figure synthesis are available from the Fairfax group bitbucket account.
